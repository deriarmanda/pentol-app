package com.karyastudio.deriarmanda.pentolapp.ui.soto

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseRvAdapter
import com.karyastudio.deriarmanda.pentolapp.base.BaseViewHolder
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.util.isPackageDone
import kotlinx.android.synthetic.main.item_package.view.*

class PackageRvAdapter(
    private val onItemClickListener: ((selected: PackageGroup) -> Unit)
) : BaseRvAdapter<PackageGroup>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<PackageGroup> {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_package, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(view: View) : BaseViewHolder<PackageGroup>(view) {
        override fun fetch(data: PackageGroup) {
            itemView.text_date.text = data.tanggal
            itemView.text_rayon.text = itemView.context.getString(
                R.string.soto_text_rayon_jumlah,
                data.rayon,
                data.jumlah
            )
            itemView.text_petugas_sudah.text =
                itemView.context.getString(R.string.soto_text_count_sudah, data.petugasSudah)
            itemView.text_petugas_belum.text =
                itemView.context.getString(R.string.soto_text_count_belum, data.petugasBelum)
            itemView.text_admin_sudah.text = itemView.context.getString(R.string.soto_text_count_sudah, data.adminSudah)
            itemView.text_admin_belum.text = itemView.context.getString(R.string.soto_text_count_belum, data.adminBelum)
            itemView.text_admin_ulangi.text =
                itemView.context.getString(R.string.soto_text_count_ulangi, data.adminUlangi)

            if (data.isPackageDone()) {
                itemView.text_status.text = itemView.context.getString(R.string.soto_text_tuntas)
                itemView.text_status.setBackgroundResource(R.color.accent)
                itemView.text_status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_white_16dp, 0, 0, 0)
                itemView.card_root.setCardBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                itemView.text_status.text = itemView.context.getString(R.string.soto_text_belum_tuntas)
                itemView.text_status.setBackgroundResource(R.color.material_red)
                itemView.text_status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_clear_white_16dp, 0, 0, 0)
                itemView.card_root.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
            }

            itemView.container.setOnClickListener { onItemClickListener(data) }
        }
    }
}