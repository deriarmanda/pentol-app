package com.karyastudio.deriarmanda.pentolapp.data.network.model

import com.google.gson.annotations.SerializedName

data class AuthUser(
    @SerializedName("username") val username: String,
    @SerializedName("password") val password: String
)

data class RequestPackageGroup(
    val group: String
)

data class RequestDetailPackageList(
    val group: String,
    val idSoTo: String
)

data class RequestDetailPackage(
    val idSoToItem: Long
)

data class UpdatePackage(
    @SerializedName("idSoToItem") val id: Long,
    @SerializedName("idPetugas") val idPetugas: Long,
    @SerializedName("sti_petugas") val namaPetugas: String,
    @SerializedName("sti_no_ba") val nomorBA: String,
    @SerializedName("hasil") val hasil: String,
    @SerializedName("stiKwhMeterSebelum") val aSebelum: String = "",
    @SerializedName("stiKwhMeterSesudah") val aSesudah: String = "",
    @SerializedName("stiKwhMerkSesudah") val aMerk: String = "",
    @SerializedName("stiKwhTahunSesudah") val aTahun: String = "",
    @SerializedName("stiKwhPutaranSesudah") val aPutaran: String = "",
    @SerializedName("stiKwhKondisiVisualSebelum") val aKondisiSebelum: String = "",
    @SerializedName("stiKwhKondisiVisualSesudah") val aKondisiSesudah: String = "",
    @SerializedName("stiSegelTerpasangSebelum") val bSebelum: String = "",
    @SerializedName("stiSegelTerpasangSesudah") val bSesudah: String = "",
    @SerializedName("stiSegelJenisSebelum") val bJenisSebelum: String = "",
    @SerializedName("stiSegelJenisSesudah") val bJenisSesudah: String = "",
    @SerializedName("stiSegelKondisiVisualSebelum") val bKondisiSebelum: String = "",
    @SerializedName("stiSegelKondisiVisualSesudah") val bKondisiSesudah: String = "",
    @SerializedName("stiPembatasSebelum") val cSebelum: String = "",
    @SerializedName("stiPembatasSesudah") val cSesudah: String = "",
    @SerializedName("stiPembatasKapasitasSebelum") val cKapasitasSebelum: String = "",
    @SerializedName("stiPembatasKapasitasSetelah") val cKapasitasSesudah: String = "",
    @SerializedName("stiPembatasMerkSebelum") val cMerkSebelum: String = "",
    @SerializedName("stiPembatasMerkSesudah") val cMerkSesudah: String = "",
    @SerializedName("stiSegel2Sebelum") val dSebelum: String = "",
    @SerializedName("stiSegel2Sesudah") val dSesudah: String = "",
    @SerializedName("stiSegel2JenisSebelum") val dJenisSebelum: String = "",
    @SerializedName("stiSegel2JenisSesudah") val dJenisSesudah: String = "",
    @SerializedName("stiPapanMeterSebelum") val eSebelum: String = "",
    @SerializedName("stiPapanMeterSesudah") val eSesudah: String = "",
    @SerializedName("stiPapanJenisSebelum") val eJenis1Sebelum: String = "",
    @SerializedName("stiPapanJenisSesudah") val eJenis1Sesudah: String = "",
    @SerializedName("stiPapanKondisiVisualSebelum") val eKondisiSebelum: String = "",
    @SerializedName("stiPapanKondisiVisualSesudah") val eKondisiSesudah: String = "",
    @SerializedName("stiPapanSegelTerpasangSebelum") val eSegelSebelum: String = "",
    @SerializedName("stiPapanSegelTerpasangSesudah") val eSegelSesudah: String = "",
    @SerializedName("stiPapan2JenisSebelum") val eJenis2Sebelum: String = "",
    @SerializedName("stiPapan2JenisSesudah") val eJenis2Sesudah: String = "",
    @SerializedName("stiPengawatanSebelum") val fSebelum: String = "",
    @SerializedName("stiPengawatanSesudah") val fSesudah: String = "",
    @SerializedName("stiKesimpulanHasilPemeriksaan") val pelanggaran: String = "",
    @SerializedName("stiTindakanYangDilakukan") val tindakan: String = "",
    @SerializedName("stiSaksiNama") val saksiNama: String = "",
    @SerializedName("stiSaksiAlamat") val saksiAlamat: String = "",
    @SerializedName("stiSaksiNomorKartuIdentitas") val saksiNomorIdentitas: String = "",
    @SerializedName("stiSaksiPekerjaan") val saksiPekerjaan: String = "",
    @SerializedName("sti_pengukuran_tegangan") val pengukuranTegangan: String = "",
    @SerializedName("sti_pengukuran_arus") val pengukuranArus: String = "",
    @SerializedName("sti_pengukuran_kw") val pengukuranKW: String = "",
    @SerializedName("sti_pengukuran_konstanta") val pengukuranKonstanta: String = "",
    @SerializedName("sti_pengukuran_put_detik") val pengukuranPutDetik: String = "",
    @SerializedName("sti_pengukuran_eror_meter") val pengukuranErrorMeter: String = "",
    @SerializedName("sti_pengukuran_kwh_kumulatif") val pengukuranKwhKumulatif: String = "",
    @SerializedName("sti_pengukuran_pemakaian_kumulatif") val pengukuranPakaiKumulatif: String = "",
    @SerializedName("sti_pengukuran_sisa_token") val pengukuranSisaToken: String = ""
)