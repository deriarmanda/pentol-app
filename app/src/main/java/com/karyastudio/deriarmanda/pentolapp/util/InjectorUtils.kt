package com.karyastudio.deriarmanda.pentolapp.util

import android.content.Context
import com.karyastudio.deriarmanda.pentolapp.config.AppExecutors
import com.karyastudio.deriarmanda.pentolapp.config.AppPreferences
import com.karyastudio.deriarmanda.pentolapp.data.local.AppDatabase
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.data.repository.PackageRepository
import com.karyastudio.deriarmanda.pentolapp.data.repository.UserRepository
import com.karyastudio.deriarmanda.pentolapp.ui.home.HomeViewModel
import com.karyastudio.deriarmanda.pentolapp.ui.login.LoginViewModel
import com.karyastudio.deriarmanda.pentolapp.ui.pending.PendingViewModel
import com.karyastudio.deriarmanda.pentolapp.ui.soto.PackageViewModel
import com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.form.FormViewModel
import com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.list.ListViewModel

object InjectorUtils {

    // Repositories
    fun getUserRepository(context: Context): UserRepository {
        val appDb = AppDatabase.getInstance(context.applicationContext)
        val preferences = AppPreferences.getInstance(context.applicationContext)
        val executors = AppExecutors.getInstance()
        return UserRepository.getInstance(
            appDb.userDao(),
            getPackageRepository(context),
            preferences,
            executors
        )
    }

    fun getPackageRepository(context: Context): PackageRepository {
        val appDb = AppDatabase.getInstance(context.applicationContext)
        val executors = AppExecutors.getInstance()
        return PackageRepository.getInstance(
            appDb.packageGroupDao(),
            appDb.packageDao(),
            executors,
            context.resources
        )
    }

    // View Model's Factories
    fun provideLoginViewModelFactory(context: Context): LoginViewModel.Factory {
        return LoginViewModel.Factory(getUserRepository(context))
    }

    fun provideHomeViewModelFactory(context: Context): HomeViewModel.Factory {
        return HomeViewModel.Factory(getUserRepository(context))
    }

    fun providePackageViewModelFactory(context: Context): PackageViewModel.Factory {
        return PackageViewModel.Factory(
            getUserRepository(context),
            getPackageRepository(context)
        )
    }

    fun providePendingViewModelFactory(context: Context): PendingViewModel.Factory {
        return PendingViewModel.Factory(getPackageRepository(context))
    }

    fun provideDetailPackageListViewModelFactory(
        context: Context,
        packageGroup: PackageGroup
    ): ListViewModel.Factory {
        return ListViewModel.Factory(getPackageRepository(context), packageGroup)
    }

    fun provideDetailPackageViewModelFactory(
        context: Context,
        detailPackage: DetailPackage
    ): FormViewModel.Factory {
        return FormViewModel.Factory(
            getPackageRepository(context),
            getUserRepository(context),
            detailPackage
        )
    }

}