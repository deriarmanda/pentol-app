package com.karyastudio.deriarmanda.pentolapp.ui.soto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseFragment
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.DetailActivity
import com.karyastudio.deriarmanda.pentolapp.util.InjectorUtils
import kotlinx.android.synthetic.main.package_fragment.*

class PackageFragment : BaseFragment<PackageViewModel>(), PackageContract {

    companion object {
        fun newInstance() = PackageFragment()
    }

    override lateinit var viewModel: PackageViewModel
    private val rvAdapter = PackageRvAdapter { openSubDetailPage(it) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.package_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(refresh_layout) {
            setColorSchemeResources(R.color.accent)
            setOnRefreshListener { refreshList() }
        }
        with(recycler_view) {
            val linearLayoutManager = LinearLayoutManager(context)
            linearLayoutManager.reverseLayout = true
            linearLayoutManager.stackFromEnd = true
            layoutManager = linearLayoutManager
            adapter = rvAdapter
        }
    }

    override fun setupViewModel() {
        val factory = InjectorUtils.providePackageViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(baseActivity!!, factory).get(PackageViewModel::class.java)
    }

    override fun setupObserver() {
        viewModel.getPackageGroups().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it.isEmpty()) showEmptyListMessage()
                else showPackageList(it)
            }
        })
        viewModel.getViewState().observe(viewLifecycleOwner, Observer { viewState ->
            setLoadingIndicator(viewState.isLoading)
            viewState.errorMessage?.let { handleError(it) }
        })
    }

    override fun showPackageList(list: List<PackageGroup>) {
        text_empty.visibility = View.GONE
        rvAdapter.list = list
    }

    override fun showEmptyListMessage() {
        rvAdapter.list = emptyList()
        text_empty.visibility = View.VISIBLE

    }

    override fun refreshList() = viewModel.refreshPackageGroups()

    override fun openSubDetailPage(selectedGroup: PackageGroup) {
        startActivity(DetailActivity.getIntent(requireActivity(), selectedGroup))
    }

    override fun handleError(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
        viewModel.onErrorHandled()
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
