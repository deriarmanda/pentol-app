package com.karyastudio.deriarmanda.pentolapp.util

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    private const val DAY_ONLY_PATTERN = "EEEE"
    private const val DATE_SHORT_PATTERN = "yyyy-MM-dd"
    private val DAY_ONLY_FORMATTER = SimpleDateFormat(DAY_ONLY_PATTERN, Locale.ENGLISH)
    private val DATE_SHORT_FORMATTER = SimpleDateFormat(DATE_SHORT_PATTERN, Locale.ENGLISH)

    fun splitDateAndTime(dateTime: String) = dateTime.split(" ")
    fun getTodayString() = DAY_ONLY_FORMATTER.format(Date())
    fun getTodayDateString() = DATE_SHORT_FORMATTER.format(Date())
}