package com.karyastudio.deriarmanda.pentolapp.ui.splashscreen

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.karyastudio.deriarmanda.pentolapp.base.BaseActivity
import com.karyastudio.deriarmanda.pentolapp.config.RC_LOGIN_PAGE
import com.karyastudio.deriarmanda.pentolapp.ui.login.LoginActivity
import com.karyastudio.deriarmanda.pentolapp.ui.main.MainActivity
import com.karyastudio.deriarmanda.pentolapp.util.InjectorUtils

class SplashScreenActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val userRepo = InjectorUtils.getUserRepository(this.applicationContext)

        if (userRepo.isUserAlreadyLogin()) {
            //InjectorUtils.getPackageRepository(this)
            startActivity(MainActivity.getIntent(this))
            finish()
        } else startActivityForResult(LoginActivity.getIntent(this), RC_LOGIN_PAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_LOGIN_PAGE && resultCode == Activity.RESULT_OK) {
            startActivity(MainActivity.getIntent(this))
        }
        finish()
    }
}
