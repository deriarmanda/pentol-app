package com.karyastudio.deriarmanda.pentolapp.ui.pending

import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage

interface PendingContract {
    fun showPendingPackages(list: List<DetailPackage>)
    fun showEmptyListMesssage()
    fun uploadAllPendingPackages()
    fun uploadSelectedPendingPackage(data: DetailPackage)
    fun handleSuccess(message: String)
    fun handleError(message: String)
    fun setLoadingIndicator(active: Boolean)
}