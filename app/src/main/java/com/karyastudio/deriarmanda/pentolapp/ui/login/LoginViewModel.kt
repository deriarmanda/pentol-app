package com.karyastudio.deriarmanda.pentolapp.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.karyastudio.deriarmanda.pentolapp.data.repository.UserRepository

class LoginViewModel(private val mUserRepo: UserRepository) : ViewModel() {

    private val loginState: MutableLiveData<LoginState> = MutableLiveData()

    init {
        loginState.value = LoginState()
    }

    fun getLoginState(): LiveData<LoginState> = loginState

    fun isUsernameAndPasswordValid(username: String, password: String): Boolean {
        return username.isNotBlank() && password.isNotBlank()
    }

    fun doLogin(username: String, password: String) {
        loginState.value = currentState().copy(isLoading = true)
        mUserRepo.login(username, password) { name, error ->
            loginState.value = currentState().copy(
                isLoading = false,
                userFullName = name,
                errorMsg = error
            )
        }
    }

    fun onErrorHandled() {
        loginState.value = currentState().copy(errorMsg = null)
    }

    private fun currentState() = loginState.value!!

    class Factory(
        private val userRepo: UserRepository
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginViewModel(userRepo) as T
        }
    }

    data class LoginState(
        val isLoading: Boolean = false,
        val userFullName: String? = null,
        val errorMsg: String? = null
    )
}
