package com.karyastudio.deriarmanda.pentolapp.util

import androidx.lifecycle.MutableLiveData
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.data.network.model.UpdatePackage

fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

fun PackageGroup.isPackageDone() = jumlah == adminSudah

//fun DetailPackage.isChecked() = hasil.isNotBlank()

fun DetailPackage.isChecked() = hasil.isNotBlank()

fun DetailPackage.convertToRequestBody(): UpdatePackage {
    return UpdatePackage(
        id = id,
        idPetugas = idPemeriksa,
        namaPetugas = namaPemeriksa,
        nomorBA = nomorBA,
        hasil = hasil,
        aSebelum = aSebelum,
        aSesudah = aSesudah,
        aMerk = aMerk,
        aTahun = aTahun,
        aPutaran = aPutaran,
        aKondisiSebelum = aKondisiSebelum,
        aKondisiSesudah = aKondisiSesudah,
        bSebelum = bSebelum,
        bSesudah = bSesudah,
        bJenisSebelum = bJenisSebelum,
        bJenisSesudah = bJenisSesudah,
        bKondisiSebelum = bKondisiSebelum,
        bKondisiSesudah = bKondisiSesudah,
        cSebelum = cSebelum,
        cSesudah = cSesudah,
        cKapasitasSebelum = cKapasitasSebelum,
        cKapasitasSesudah = cKapasitasSesudah,
        cMerkSebelum = cMerkSebelum,
        cMerkSesudah = cMerkSesudah,
        dSebelum = dSebelum,
        dSesudah = dSesudah,
        dJenisSebelum = dJenisSebelum,
        dJenisSesudah = dJenisSesudah,
        eSebelum = eSebelum,
        eSesudah = eSesudah,
        eJenis1Sebelum = eJenis1Sebelum,
        eJenis1Sesudah = eJenis1Sesudah,
        eKondisiSebelum = eKondisiSebelum,
        eKondisiSesudah = eKondisiSesudah,
        eSegelSebelum = eSegelSebelum,
        eSegelSesudah = eSegelSesudah,
        eJenis2Sebelum = eJenis2Sebelum,
        eJenis2Sesudah = eJenis2Sesudah,
        fSebelum = fSebelum,
        fSesudah = fSesudah,
        pelanggaran = pelanggaran,
        tindakan = tindakan,
        saksiNama = saksiNama,
        saksiAlamat = saksiAlamat,
        saksiNomorIdentitas = saksiNomorIdentitas,
        saksiPekerjaan = saksiPekerjaan,
        pengukuranTegangan = pengukuranTegangan,
        pengukuranArus = pengukuranArus,
        pengukuranKW = pengukuranKW,
        pengukuranKonstanta = pengukuranKonstanta,
        pengukuranPutDetik = pengukuranPutDetik,
        pengukuranErrorMeter = pengukuranErrorMeter,
        pengukuranKwhKumulatif = pengukuranKwhKumulatif,
        pengukuranPakaiKumulatif = pengukuranPakaiKumulatif,
        pengukuranSisaToken = pengukuranSisaToken
    )
}