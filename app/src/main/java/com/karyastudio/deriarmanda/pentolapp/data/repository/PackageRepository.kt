package com.karyastudio.deriarmanda.pentolapp.data.repository

import android.content.res.Resources
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.config.AppExecutors
import com.karyastudio.deriarmanda.pentolapp.data.local.dao.PackageDao
import com.karyastudio.deriarmanda.pentolapp.data.local.dao.PackageGroupDao
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.data.network.ClientBuilder
import com.karyastudio.deriarmanda.pentolapp.data.network.model.*
import com.karyastudio.deriarmanda.pentolapp.data.network.service.PackageService
import com.karyastudio.deriarmanda.pentolapp.util.convertToRequestBody
import com.karyastudio.deriarmanda.pentolapp.util.default
import id.zelory.compressor.Compressor
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException

class PackageRepository private constructor(
    private val packageGroupDao: PackageGroupDao,
    private val packageDao: PackageDao,
    private val executors: AppExecutors,
    private val resources: Resources
) {

    private val packageService = ClientBuilder.build(PackageService::class.java)
    private val networkPackageGroups = MutableLiveData<List<PackageGroup>>().default(emptyList())
    private val networkDetailPackages = MutableLiveData<List<DetailPackage>>().default(emptyList())

    init {
        networkPackageGroups.observeForever {
            if (it != null && it.isNotEmpty()) updateLocalPackageGroups(it)
        }
        networkDetailPackages.observeForever {
            if (it != null && it.isNotEmpty()) updateLocalDetailPackages(it)
        }
    }

    companion object {

        private const val TAG = "PackageRepository"
        @Volatile
        private var instance: PackageRepository? = null

        fun getInstance(
            packageGroupDao: PackageGroupDao,
            packageDao: PackageDao,
            executors: AppExecutors,
            resources: Resources
        ): PackageRepository {
            return instance ?: synchronized(this) {
                instance ?: PackageRepository(
                    packageGroupDao, packageDao, executors, resources
                ).also { instance = it }
            }
        }
    }

    fun getLocalPackageGroups() = packageGroupDao.getPackageGroups()

    fun getLocalDetailPackagesList(groupId: Long) = packageDao.getPackagesByGroupId(groupId)

    fun getPendingPackagesList() = packageDao.getPendingPackages()

    fun refreshPackageGroups(userGroup: String, onFinished: (error: String?) -> Unit) {
        val body = RequestPackageGroup(userGroup)
        packageService.getPackageGroups(body).enqueue(
            object : Callback<Common<List<PackageGroupsResponse>>> {
                override fun onFailure(call: Call<Common<List<PackageGroupsResponse>>>, t: Throwable) {
                    Log.d(TAG, "onFailure -> $t")
                    if (t is IOException) onFinished(resources.getString(R.string.error_no_internet))
                    else onFinished(resources.getString(R.string.error_unknown))
                }

                override fun onResponse(
                    call: Call<Common<List<PackageGroupsResponse>>>,
                    response: Response<Common<List<PackageGroupsResponse>>>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()?.data
                        if (data != null) {
                            val list = arrayListOf<PackageGroup>()
                            for (datum in data) list.add(datum.convertToLocalPackageGroupModel())

                            networkPackageGroups.value = list
                            onFinished(null)
                        } else {
                            onFinished(
                                response.body()?.message ?: resources.getString(R.string.error_unknown)
                            )
                        }
                    } else {
                        onFinished(resources.getString(R.string.error_unknown))
                    }
                }
            }
        )
    }

    fun refreshDetailPackages(userGroup: String, groupId: Long, onFinished: (error: String?) -> Unit) {
        val body = RequestDetailPackageList(userGroup, groupId.toString())
        packageService.getDetailPackagesList(body).enqueue(
            object : Callback<Common<List<DetailPackageResponse>>> {
                override fun onFailure(call: Call<Common<List<DetailPackageResponse>>>, t: Throwable) {
                    Log.d(TAG, "onFailure -> $t")
                    if (t is IOException) onFinished(resources.getString(R.string.error_no_internet))
                    else onFinished(resources.getString(R.string.error_unknown))
                }

                override fun onResponse(
                    call: Call<Common<List<DetailPackageResponse>>>,
                    listResponse: Response<Common<List<DetailPackageResponse>>>
                ) {
                    if (listResponse.isSuccessful) {
                        val data = listResponse.body()?.data
                        if (data != null) {
                            val list = arrayListOf<DetailPackage>()
                            for (datum in data) list.add(datum.convertToLocalDetailPackageModel())

                            networkDetailPackages.value = list
                            onFinished(null)
                        } else {
                            onFinished(
                                listResponse.body()?.message ?: resources.getString(R.string.error_unknown)
                            )
                        }
                    } else {
                        onFinished(resources.getString(R.string.error_unknown))
                    }
                }
            }
        )
    }

    fun updatePackage(data: DetailPackage, compressor: Compressor, onFinished: (error: String?) -> Unit) {
        executors.diskIO.execute {
            packageDao.save(data)
            executors.networkIO.execute {
                val service = prepareUpdateService(data, compressor)
                service.enqueue(
                    object : Callback<Common<Any>> {
                        override fun onFailure(call: Call<Common<Any>>, t: Throwable) {
                            Log.d(TAG, "onFailure -> $t")
                            updateDataPendingStatus(data, true)
                            if (t is IOException) onFinished(resources.getString(R.string.update_error_no_internet))
                            else onFinished(resources.getString(R.string.update_error_unknown))
                        }

                        override fun onResponse(call: Call<Common<Any>>, response: Response<Common<Any>>) {
                            if (response.isSuccessful) {
                                val body = response.body()
                                if (body != null) {
                                    if (body.status == "success") {
                                        updateDataPendingStatus(data, false)
                                        onFinished(null)
                                    } else {
                                        updateDataPendingStatus(data, true)
                                        onFinished(body.message)
                                    }
                                } else {
                                    Log.d(TAG, "onResponse -> ${response.raw()}")
                                    updateDataPendingStatus(data, true)
                                    onFinished(resources.getString(R.string.update_error_unknown))
                                }
                            } else {
                                Log.d(TAG, "onResponse -> ${response.raw()}")
                                updateDataPendingStatus(data, true)
                                onFinished(resources.getString(R.string.update_error_unknown))
                            }
                        }
                    }
                )
            }
        }
    }

    fun reuploadPendingPackage(
        data: DetailPackage,
        compressor: Compressor,
        onFinished: (success: String?, error: String?) -> Unit
    ) {
        val service = prepareUpdateService(data, compressor)
        service.enqueue(
            object : Callback<Common<Any>> {
                override fun onFailure(call: Call<Common<Any>>, t: Throwable) {
                    Log.d(TAG, "onFailure -> $t")
                    updateDataPendingStatus(data, true)
                    onFinished(
                        null,
                        resources.getString(
                            R.string.pending_error_reupload_with_name,
                            data.pelangganNama
                        )
                    )
                }

                override fun onResponse(call: Call<Common<Any>>, response: Response<Common<Any>>) {
                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body != null) {
                            if (body.status == "success") {
                                updateDataPendingStatus(data, false)
                                onFinished(
                                    resources.getString(
                                        R.string.pending_msg_reupload_selected_with_name_success,
                                        data.pelangganNama
                                    ), null
                                )
                            } else {
                                updateDataPendingStatus(data, true)
                                onFinished(
                                    body.message,
                                    resources.getString(
                                        R.string.pending_error_reupload_with_name,
                                        data.pelangganNama
                                    )
                                )
                            }
                        } else {
                            Log.d(TAG, "onResponse -> ${response.raw()}")
                            updateDataPendingStatus(data, true)
                            onFinished(
                                null,
                                resources.getString(
                                    R.string.pending_error_reupload_with_name,
                                    data.pelangganNama
                                )
                            )
                        }
                    } else {
                        Log.d(TAG, "onResponse -> ${response.raw()}")
                        updateDataPendingStatus(data, true)
                        onFinished(
                            null,
                            resources.getString(
                                R.string.pending_error_reupload_with_name,
                                data.pelangganNama
                            )
                        )
                    }
                }
            }
        )
    }

    fun reuploadPendingPackage(compressor: Compressor, onFinished: (success: String?, error: String?) -> Unit) {
        executors.diskIO.execute {
            val list = packageDao.getPendingPackagesWithoutLiveData()
            for (data in list) {
                executors.networkIO.execute {
                    val service = prepareUpdateService(data, compressor)
                    service.enqueue(
                        object : Callback<Common<Any>> {
                            override fun onFailure(call: Call<Common<Any>>, t: Throwable) {
                                Log.d(TAG, "onFailure -> $t")
                                updateDataPendingStatus(data, true)
                                onFinished(
                                    null,
                                    resources.getString(
                                        R.string.pending_error_reupload_with_name,
                                        data.pelangganNama
                                    )
                                )
                            }

                            override fun onResponse(call: Call<Common<Any>>, response: Response<Common<Any>>) {
                                if (response.isSuccessful) {
                                    val body = response.body()
                                    if (body != null) {
                                        if (body.status == "success") {
                                            updateDataPendingStatus(data, false)
                                            onFinished(
                                                resources.getString(
                                                    R.string.pending_msg_reupload_selected_with_name_success,
                                                    data.pelangganNama
                                                ), null
                                            )
                                        } else {
                                            updateDataPendingStatus(data, true)
                                            onFinished(
                                                body.message,
                                                resources.getString(
                                                    R.string.pending_error_reupload_with_name,
                                                    data.pelangganNama
                                                )
                                            )
                                        }
                                    } else {
                                        Log.d(TAG, "onResponse -> ${response.raw()}")
                                        updateDataPendingStatus(data, true)
                                        onFinished(
                                            null,
                                            resources.getString(
                                                R.string.pending_error_reupload_with_name,
                                                data.pelangganNama
                                            )
                                        )
                                    }
                                } else {
                                    Log.d(TAG, "onResponse -> ${response.raw()}")
                                    updateDataPendingStatus(data, true)
                                    onFinished(
                                        null,
                                        resources.getString(
                                            R.string.pending_error_reupload_with_name,
                                            data.pelangganNama
                                        )
                                    )
                                }
                            }
                        }
                    )
                }
            }
        }
    }

    fun clearAllLocalData() {
        packageGroupDao.removeAll()
    }

    private fun updateLocalPackageGroups(list: List<PackageGroup>) {
        executors.diskIO.execute {
            packageGroupDao.saveAll(list)
        }
    }

    private fun updateLocalDetailPackages(list: List<DetailPackage>) {
        executors.diskIO.execute {
            for (data in list) {
                val oldData = packageDao.getPendingPackageById(data.id)
                if (oldData != null && oldData.isPending) continue
                else packageDao.save(data)
            }
        }
    }

    private fun updateDataPendingStatus(data: DetailPackage, status: Boolean) {
        executors.diskIO.execute {
            packageDao.updatePendingStatusById(data.id, status)
        }
    }

    private fun prepareUpdateService(data: DetailPackage, compressor: Compressor): Call<Common<Any>> {
        val part1 = prepareFilePart("foto1", data.pathFoto1, compressor)!!
        val part2 = prepareFilePart("foto2", data.pathFoto2, compressor)!!
        val part3 = prepareFilePart("foto3", data.pathFoto3, compressor)!!

        return packageService.updatePackage(data.convertToRequestBody(), part1, part2, part3)
        /*return if (part1 != null && part2 != null && part3 != null) {
            Log.d(TAG, "send 3 foto")
            packageService.updatePackage(data.convertToRequestBody(), part1, part2, part3)
        } else if (part1 != null && part2 != null) {
            packageService.updatePackage(data.convertToRequestBody(), part1, part2)
        } else if (part1 != null && part3 != null) {
            packageService.updatePackage(data.convertToRequestBody(), part1, part3)
        } else if (part2 != null && part3 != null) {
            packageService.updatePackage(data.convertToRequestBody(), part2, part3)
        } else if (part1 != null) {
            packageService.updatePackage(data.convertToRequestBody(), part1)
        } else if (part2 != null) {
            packageService.updatePackage(data.convertToRequestBody(), part2)
        } else if (part3 != null) {
            packageService.updatePackage(data.convertToRequestBody(), part3)
        } else {
            packageService.updatePackage(data.convertToRequestBody())
        }*/
    }

    private fun prepareFilePart(partName: String, filePath: String, compressor: Compressor): MultipartBody.Part? {
        val file = File(filePath)
        return if (!file.exists()) null
        else {
            //val compressed = compressor.compressToFile(file)
            val request = RequestBody.create(MediaType.parse("image/*"), file)
            MultipartBody.Part.createFormData(partName, file.name, request)
        }
    }
}