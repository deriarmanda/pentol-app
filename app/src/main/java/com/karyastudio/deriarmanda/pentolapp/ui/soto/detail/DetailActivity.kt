package com.karyastudio.deriarmanda.pentolapp.ui.soto.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseActivity
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.form.FormActivity
import com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.list.ListFragment

class DetailActivity : BaseActivity() {

    lateinit var packageGroup: PackageGroup

    companion object {

        private const val EXTRA_GROUP = "extra_package_group"

        fun getIntent(context: Context, group: PackageGroup): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(EXTRA_GROUP, group)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        packageGroup = intent.getParcelableExtra(EXTRA_GROUP)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.root, ListFragment.newInstance())
                .commitNow()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    fun gotoFormPage(selectedPackage: DetailPackage) {
        startActivity(FormActivity.getIntent(this, selectedPackage))
    }
}
