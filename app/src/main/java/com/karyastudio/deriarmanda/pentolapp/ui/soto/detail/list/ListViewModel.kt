package com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.karyastudio.deriarmanda.pentolapp.base.BaseViewState
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.data.repository.PackageRepository
import com.karyastudio.deriarmanda.pentolapp.util.default

class ListViewModel(
    private val mPackageRepo: PackageRepository,
    packageGroup: PackageGroup
) : ViewModel() {

    private val mPackageGroup = MutableLiveData<PackageGroup>().default(packageGroup)
    private val viewState = MutableLiveData<BaseViewState>().default(BaseViewState())

    fun getPackageGroup(): LiveData<PackageGroup> = mPackageGroup
    fun getViewState(): LiveData<BaseViewState> = viewState

    fun getPackageList(): LiveData<List<DetailPackage>> {
        val groupId = mPackageGroup.value?.id ?: -1
        return mPackageRepo.getLocalDetailPackagesList(groupId)
    }

    fun refreshPackageList() {
        viewState.value = currentState().copy(isLoading = true)
        val userGroup = mPackageGroup.value?.group ?: "-"
        val groupId = mPackageGroup.value?.id ?: -1

        mPackageRepo.refreshDetailPackages(userGroup, groupId) {
            viewState.value = currentState().copy(
                isLoading = false,
                errorMessage = it
            )
        }
    }

    fun onErrorHandled() {
        viewState.value = currentState().copy(errorMessage = null)
    }

    private fun currentState() = viewState.value!!

    class Factory(
        private val packageRepo: PackageRepository,
        private val group: PackageGroup
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ListViewModel(packageRepo, group) as T
        }
    }
}
