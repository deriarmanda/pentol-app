package com.karyastudio.deriarmanda.pentolapp.data.network.service

import com.karyastudio.deriarmanda.pentolapp.data.network.model.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface PackageService {

    @POST("packageSoToByGroup")
    fun getPackageGroups(@Body group: RequestPackageGroup): Call<Common<List<PackageGroupsResponse>>>

    @POST("packageDataSoToByGroup")
    fun getDetailPackagesList(@Body body: RequestDetailPackageList): Call<Common<List<DetailPackageResponse>>>

    @Deprecated("Use getDetailPackagesList instead.")
    @POST("packageDataSoToByIdItem")
    fun getDetailPackage(@Body body: RequestDetailPackage): Call<Common<List<DetailPackageResponse>>>

    @Headers("Enctype: multipart/form-data")
    @Multipart
    @POST("doSoToById")
    fun updatePackage(
        @Part("data") data: UpdatePackage,
        @Part foto1: MultipartBody.Part,
        @Part foto2: MultipartBody.Part,
        @Part foto3: MultipartBody.Part
    ): Call<Common<Any>>

    /*//@Headers("Content-Type: multipart/form-data")
    @Multipart
    @POST("doSoToById")
    fun updatePackage(
        @Part("data") data: UpdatePackage,
        @Part foto1: MultipartBody.Part,
        @Part foto2: MultipartBody.Part
    ): Call<Common<Any>>

    //@Headers("Content-Type: multipart/form-data")
    @Multipart
    @POST("doSoToById")
    fun updatePackage(
        @Part("data") data: UpdatePackage,
        @Part foto1: MultipartBody.Part
    ): Call<Common<Any>>

    @POST("doSoToById")
    fun updatePackage(
        @Body data: UpdatePackage
    ): Call<Common<Any>>*/
}