package com.karyastudio.deriarmanda.pentolapp.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(
    @PrimaryKey val id: Int,
    val username: String,
    val name: String,
    val level: String,
    val group: String,
    val rayon: String,
    val status: String
)