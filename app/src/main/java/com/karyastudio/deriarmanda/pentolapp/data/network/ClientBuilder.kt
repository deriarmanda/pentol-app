package com.karyastudio.deriarmanda.pentolapp.data.network

import android.util.Log
import com.google.gson.GsonBuilder
import com.karyastudio.deriarmanda.pentolapp.BuildConfig
import com.karyastudio.deriarmanda.pentolapp.config.BASE_URL
import com.karyastudio.deriarmanda.pentolapp.data.network.interceptor.DefaultHeader
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ClientBuilder {
    fun <T> build(clazz: Class<T>): T {
        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(getHttpClient(clazz.simpleName))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(clazz)
    }

    private fun getHttpClient(tag: String): OkHttpClient {
        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Log.d(tag, it)
        })
        logger.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.BASIC

        val builder = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(DefaultHeader())
            .addInterceptor(logger)
        return builder.build()
    }
}