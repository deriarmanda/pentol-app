package com.karyastudio.deriarmanda.pentolapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseFragment
import com.karyastudio.deriarmanda.pentolapp.data.local.model.User
import com.karyastudio.deriarmanda.pentolapp.util.InjectorUtils
import kotlinx.android.synthetic.main.home_fragment.*

class HomeFragment : BaseFragment<HomeViewModel>(), HomeContract {

    companion object {
        fun newInstance() = HomeFragment()
    }

    override lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun setupViewModel() {
        val factory = InjectorUtils.provideHomeViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(baseActivity!!, factory).get(HomeViewModel::class.java)
    }

    override fun setupObserver() {
        viewModel.userProfile.observe(viewLifecycleOwner, Observer { if (it != null) showUserProfile(it) })
    }

    override fun showUserProfile(user: User) {
        text_username.text = user.username
        text_name.text = user.name
        text_level.text = user.level
        text_group.text = user.group
        text_rayon.text = user.rayon
        text_status.text = user.status
    }

    override fun showChart() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
