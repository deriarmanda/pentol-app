package com.karyastudio.deriarmanda.pentolapp.base

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment

abstract class BaseFragment<VM> : Fragment() {

    abstract val viewModel: VM
    var baseActivity: BaseActivity? = null

    abstract fun setupViewModel()
    abstract fun setupObserver()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) baseActivity = context
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewModel()
        setupObserver()
    }

    override fun onDetach() {
        super.onDetach()
        baseActivity = null
    }

    fun hideKeyboard() = baseActivity?.hideKeyboard()
    fun isNetworkAvailable() = baseActivity?.isNetworkAvailable() ?: false
}