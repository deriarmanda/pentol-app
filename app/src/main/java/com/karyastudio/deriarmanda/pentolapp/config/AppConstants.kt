package com.karyastudio.deriarmanda.pentolapp.config

// Database
const val DATABASE_NAME = "pentol_app_db"
const val DATABASE_VERSION = 6

// Network
const val API_RELEASE = "http://tranel.ip-dynamic.com/pentol"
const val API_PREFIX = "webservice"
const val API_VERSION = "v1"
const val BASE_URL = "$API_RELEASE/$API_PREFIX/$API_VERSION/"

// Shared Preferences
const val PREFERENCES_NAME = "pentol_app_preferences"
const val PREF_KEY_USER_STATUS = "user_status"

// Intent Request Code
const val RC_LOGIN_PAGE = 1
const val RC_PICK_IMAGE_1 = 2
const val RC_PICK_IMAGE_2 = 3
const val RC_PICK_IMAGE_3 = 4
