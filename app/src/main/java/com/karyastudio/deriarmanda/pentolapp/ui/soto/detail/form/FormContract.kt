package com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.form

import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage

interface FormContract {
    fun setFormState(isShown: Boolean)
    fun setCardPemeriksaanState(isExpand: Boolean)
    fun setCardSaksiState(isExpand: Boolean)
    fun setCardPengukuranState(isExpand: Boolean)
    fun showPackage(data: DetailPackage)
    fun processUpdate()
    fun openMapsApp(lat: String, long: String, label: String)
    fun pickImage(requestCode: Int)
    fun onProcessUpdateError(message: String)
    fun setLoadingIndicator(active: Boolean)
}