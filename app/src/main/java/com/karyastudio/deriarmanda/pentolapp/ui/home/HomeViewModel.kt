package com.karyastudio.deriarmanda.pentolapp.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.karyastudio.deriarmanda.pentolapp.data.local.model.User
import com.karyastudio.deriarmanda.pentolapp.data.repository.UserRepository

class HomeViewModel(mUserRepo: UserRepository) : ViewModel() {

    val userProfile: LiveData<User> = mUserRepo.getObservableUserProfile()

    class Factory(
        private val userRepo: UserRepository
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return HomeViewModel(userRepo) as T
        }
    }
}
