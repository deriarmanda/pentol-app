package com.karyastudio.deriarmanda.pentolapp.ui.login

interface LoginContract {
    fun doLogin()
    fun onLoginSuccess(welcomeMsg: String)
    fun onLoginError(errorMsg: String)
}