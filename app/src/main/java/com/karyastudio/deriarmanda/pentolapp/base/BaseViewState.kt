package com.karyastudio.deriarmanda.pentolapp.base

data class BaseViewState(
    val isLoading: Boolean = false,
    val errorMessage: String? = null
)