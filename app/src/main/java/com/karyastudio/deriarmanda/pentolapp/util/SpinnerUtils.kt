package com.karyastudio.deriarmanda.pentolapp.util

import android.content.res.Resources
import com.karyastudio.deriarmanda.pentolapp.R

object SpinnerUtils {

    fun findHasilSpinnerIndexByValue(res: Resources, value: String): Int {
        val values = res.getStringArray(R.array.update_entries_hasil)
        for ((index, item) in values.withIndex()) {
            if (item == value) return index
        }
        return 0
    }

    fun findAdaTidakSpinnerIndexByValue(res: Resources, value: String): Int {
        val values = res.getStringArray(R.array.update_entries_ada_tidak)
        for ((index, item) in values.withIndex()) {
            if (item == value) return index
        }
        return 0
    }

    fun findBaikTidakSpinnerIndexByValue(res: Resources, value: String): Int {
        val values = res.getStringArray(R.array.update_entries_baik_tidak)
        for ((index, item) in values.withIndex()) {
            if (item == value) return index
        }
        return 0
    }

    fun findPakaiTidakSpinnerIndexByValue(res: Resources, value: String): Int {
        val values = res.getStringArray(R.array.update_entries_pakai_tidak)
        for ((index, item) in values.withIndex()) {
            if (item == value) return index
        }
        return 0
    }

    fun findSesuaiTidakSpinnerIndexByValue(res: Resources, value: String): Int {
        val values = res.getStringArray(R.array.update_entries_sesuai_tidak)
        for ((index, item) in values.withIndex()) {
            if (item == value) return index
        }
        return 0
    }

    fun findPlastikMetalSpinnerIndexByValue(res: Resources, value: String): Int {
        val values = res.getStringArray(R.array.update_entries_plastik_metal)
        for ((index, item) in values.withIndex()) {
            if (item == value) return index
        }
        return 0
    }
}