package com.karyastudio.deriarmanda.pentolapp.data.network.service

import com.karyastudio.deriarmanda.pentolapp.data.network.model.AuthUser
import com.karyastudio.deriarmanda.pentolapp.data.network.model.Common
import com.karyastudio.deriarmanda.pentolapp.data.network.model.UserInfo
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UserService {

    @POST("authPetugas")
    fun authenticate(@Body user: AuthUser): Call<Common<UserInfo>>
}