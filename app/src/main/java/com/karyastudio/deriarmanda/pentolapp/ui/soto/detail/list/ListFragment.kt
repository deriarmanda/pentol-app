package com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.list

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseFragment
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.DetailActivity
import com.karyastudio.deriarmanda.pentolapp.util.InjectorUtils
import com.karyastudio.deriarmanda.pentolapp.util.isPackageDone
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import kotlinx.android.synthetic.main.list_fragment.*

class ListFragment : BaseFragment<ListViewModel>(), ListContract {

    private var detailActivity: DetailActivity? = null

    companion object {
        fun newInstance() = ListFragment()
    }

    override lateinit var viewModel: ListViewModel
    private val onItemButtonClickListener = object : ListRvAdapter.OnButtonClickListener {
        override fun onEditButtonClick(selected: DetailPackage) = openFormDetailPage(selected)
        override fun onMapsButtonClick(selected: DetailPackage) = openMapsApp(selected)
    }
    private val rvAdapter = ListRvAdapter(onItemButtonClickListener)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        detailActivity = baseActivity as DetailActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(refresh_layout) {
            setColorSchemeResources(R.color.accent)
            setOnRefreshListener { refreshList() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = rvAdapter
        }
    }

    override fun onDetach() {
        super.onDetach()
        detailActivity = null
    }

    override fun setupViewModel() {
        val group = detailActivity!!.packageGroup
        val factory = InjectorUtils.provideDetailPackageListViewModelFactory(requireContext(), group)
        viewModel = ViewModelProviders.of(detailActivity!!, factory).get(ListViewModel::class.java)
    }

    override fun setupObserver() {
        viewModel.getPackageGroup().observe(viewLifecycleOwner, Observer { showPackageInfo(it) })
        viewModel.getPackageList().observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) showEmptyListMessage()
            else showSoToList(it)
        })
        viewModel.getViewState().observe(viewLifecycleOwner, Observer { state ->
            setLoadingIndicator(state.isLoading)
            state.errorMessage?.let { handleError(it) }
        })
    }

    override fun showPackageInfo(data: PackageGroup) {
        text_tanggal.text = data.tanggal
        text_rayon.text = getString(R.string.soto_text_rayon_full, data.rayon, data.kodeRayon)
        text_jumlah.text = getString(R.string.soto_text_rayon_jumlah, data.group, data.jumlah)
        text_petugas_sudah.text = getString(R.string.soto_text_count_sudah, data.petugasSudah)
        text_petugas_belum.text = getString(R.string.soto_text_count_belum, data.petugasBelum)
        text_admin_sudah.text = getString(R.string.soto_text_count_sudah, data.adminSudah)
        text_admin_belum.text = getString(R.string.soto_text_count_belum, data.adminBelum)
        text_admin_ulangi.text = getString(R.string.soto_text_count_ulangi, data.adminUlangi)

        if (data.isPackageDone()) {
            text_status.text = getString(R.string.soto_text_tuntas)
            text_status.setBackgroundResource(R.color.accent)
            text_status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_done_all_white_16dp, 0, 0, 0)
        } else {
            text_status.text = getString(R.string.soto_text_belum_tuntas)
            text_status.setBackgroundResource(R.color.material_red)
            text_status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_clear_white_16dp, 0, 0, 0)
        }
    }

    override fun showSoToList(list: List<DetailPackage>) {
        text_empty.visibility = View.GONE
        rvAdapter.list = list
    }

    override fun showEmptyListMessage() {
        rvAdapter.list = emptyList()
        text_empty.visibility = View.VISIBLE
    }

    override fun refreshList() = viewModel.refreshPackageList()

    override fun openFormDetailPage(selected: DetailPackage) {
        detailActivity?.gotoFormPage(selected)
    }

    override fun openMapsApp(selected: DetailPackage) {
        val uri = Uri.parse(
            "geo:${selected.pelangganLatitude},${selected.pelangganLongitude}?q=" +
                    "${selected.pelangganLatitude},${selected.pelangganLongitude}(${selected.pelangganAlamat})"
        )
        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.setPackage("com.google.android.apps.maps")

        if (intent.resolveActivity(detailActivity!!.packageManager) != null) {
            startActivity(intent)
        } else {
            showMapsNotFoundErrorDialog()
        }
    }

    override fun handleError(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
        viewModel.onErrorHandled()
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }

    private fun showMapsNotFoundErrorDialog() {
        val dialog = LovelyStandardDialog(requireContext())
            .setTopColorRes(R.color.material_red)
            .setButtonsColorRes(R.color.accent)
            .setIcon(R.drawable.ic_error_outline_white_36dp)
            .setTitle(R.string.soto_error_maps_title)
            .setMessage(R.string.soto_error_maps_message)
            .setCancelable(true)

        dialog.setPositiveButton(R.string.action_ok) {
            dialog.dismiss()
        }.show()
    }
}
