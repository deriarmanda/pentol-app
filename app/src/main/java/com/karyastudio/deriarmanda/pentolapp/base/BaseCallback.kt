package com.karyastudio.deriarmanda.pentolapp.base

interface BaseCallback {
    fun onSuccess()
    fun onError(message: String)
}