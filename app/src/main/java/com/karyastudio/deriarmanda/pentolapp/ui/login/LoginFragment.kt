package com.karyastudio.deriarmanda.pentolapp.ui.login

import android.app.Activity.RESULT_OK
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseFragment
import com.karyastudio.deriarmanda.pentolapp.util.InjectorUtils
import kotlinx.android.synthetic.main.login_fragment.*

class LoginFragment : BaseFragment<LoginViewModel>(), LoginContract {

    companion object {
        fun newInstance() = LoginFragment()
    }

    override lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_login.setOnClickListener { doLogin() }
    }

    override fun setupViewModel() {
        val factory = InjectorUtils.provideLoginViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(baseActivity!!, factory).get(LoginViewModel::class.java)
    }

    override fun setupObserver() {
        viewModel.getLoginState().observe(viewLifecycleOwner, Observer { state ->
            setLoadingIndicator(state.isLoading)
            state.userFullName?.let { onLoginSuccess(getString(R.string.login_msg_success, it)) }
            state.errorMsg?.let { onLoginError(it) }
        })
    }

    override fun doLogin() {
        val username = form_username.text.toString()
        val password = form_password.text.toString()
        if (viewModel.isUsernameAndPasswordValid(username, password)) viewModel.doLogin(username, password)
        else Toast.makeText(requireContext(), R.string.login_error_validation, Toast.LENGTH_LONG).show()
    }

    override fun onLoginSuccess(welcomeMsg: String) {
        InjectorUtils.getPackageRepository(requireContext())
        Toast.makeText(requireContext(), welcomeMsg, Toast.LENGTH_LONG).show()
        baseActivity?.setResult(RESULT_OK)
        baseActivity?.finish()
    }

    override fun onLoginError(errorMsg: String) {
        Toast.makeText(requireContext(), errorMsg, Toast.LENGTH_LONG).show()
        viewModel.onErrorHandled()
    }

    private fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
        button_login.isClickable = !active
    }
}
