package com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseRvAdapter
import com.karyastudio.deriarmanda.pentolapp.base.BaseViewHolder
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.util.DateTimeUtils
import com.karyastudio.deriarmanda.pentolapp.util.isChecked
import kotlinx.android.synthetic.main.item_detail_package.view.*

class ListRvAdapter(
    private val onButtonClickListener: OnButtonClickListener
) : BaseRvAdapter<DetailPackage>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<DetailPackage> {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_detail_package, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(view: View) : BaseViewHolder<DetailPackage>(view) {
        override fun fetch(data: DetailPackage) {
            val res = itemView.context.resources
            itemView.text_nama_pelanggan.text = res.getString(
                R.string.soto_text_nama_kode_pelanggan,
                data.pelangganNama, data.pelangganKode
            )
            itemView.text_alamat_pelanggan.text = data.pelangganAlamat

            if (data.isChecked()) {
                itemView.text_pemeriksa.text = res.getString(
                    R.string.soto_text_telah_diperiksa,
                    data.namaPemeriksa,
                    DateTimeUtils.splitDateAndTime(data.tanggalPeriksa)[0]
                )
                itemView.image_status.setImageResource(R.drawable.ic_check_white_24dp)
                itemView.image_status.setBackgroundResource(R.drawable.bg_circle_blue)
                itemView.line_left.setBackgroundResource(R.drawable.bg_left_item_package_blue)
                //itemView.setBackgroundResource(R.color.plain)
            } else {
                itemView.text_pemeriksa.text = res.getString(R.string.soto_text_belum_diperiksa)
                itemView.image_status.setImageResource(R.drawable.ic_clear_white_24dp)
                itemView.image_status.setBackgroundResource(R.drawable.bg_circle_red)
                itemView.line_left.setBackgroundResource(R.drawable.bg_left_item_package_red)
                //itemView.setBackgroundResource(R.color.white)
            }

            itemView.button_edit.setOnClickListener { onButtonClickListener.onEditButtonClick(data) }
            itemView.button_maps.setOnClickListener { onButtonClickListener.onMapsButtonClick(data) }
        }
    }

    interface OnButtonClickListener {
        fun onEditButtonClick(selected: DetailPackage)
        fun onMapsButtonClick(selected: DetailPackage)
    }
}