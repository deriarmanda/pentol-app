package com.karyastudio.deriarmanda.pentolapp.ui.pending

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseFragment
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.util.InjectorUtils
import kotlinx.android.synthetic.main.pending_fragment.*

class PendingFragment : BaseFragment<PendingViewModel>(), PendingContract {

    companion object {
        fun newInstance() = PendingFragment()
    }

    override lateinit var viewModel: PendingViewModel
    private val rvAdapter = PendingRvAdapter { uploadSelectedPendingPackage(it) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pending_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(refresh_layout) {
            setColorSchemeResources(R.color.accent)
            setOnRefreshListener { setLoadingIndicator(false) }
        }
        with(list_pending) {
            layoutManager = LinearLayoutManager(context)
            adapter = rvAdapter
        }
        fab_upload.setOnClickListener { uploadAllPendingPackages() }
    }

    override fun setupViewModel() {
        val factory = InjectorUtils.providePendingViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(baseActivity!!, factory).get(PendingViewModel::class.java)
    }

    override fun setupObserver() {
        viewModel.getViewState().observe(viewLifecycleOwner, Observer { state ->
            setLoadingIndicator(state.isLoading)
            state.successMessage?.let { handleSuccess(it) }
            state.errorMessage?.let { handleError(it) }
        })
        viewModel.getPendingPackages().observe(viewLifecycleOwner, Observer { list ->
            list?.let {
                if (it.isEmpty()) showEmptyListMesssage()
                else showPendingPackages(it)
            }
        })
    }

    override fun showPendingPackages(list: List<DetailPackage>) {
        text_empty.visibility = View.GONE
        fab_upload.show()
        rvAdapter.list = list
    }

    override fun showEmptyListMesssage() {
        rvAdapter.list = emptyList()
        fab_upload.hide()
        text_empty.visibility = View.VISIBLE
    }

    override fun uploadAllPendingPackages() {
        viewModel.reuploadAllPackages(requireContext())
    }

    override fun uploadSelectedPendingPackage(data: DetailPackage) {
        viewModel.reuploadSelectedPackage(requireContext(), data)
    }

    override fun handleSuccess(message: String) {
        viewModel.onSuccessHandled()
        val msg =
            if (message.isEmpty()) getString(R.string.pending_msg_reupload_selected_success)
            else message
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }

    override fun handleError(message: String) {
        viewModel.onErrorHandled()
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active

        if (active) fab_upload.hide()
        else {
            if (text_empty.visibility == View.VISIBLE) fab_upload.hide()
            else fab_upload.show()
        }
    }
}
