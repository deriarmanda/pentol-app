package com.karyastudio.deriarmanda.pentolapp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage

@Dao
interface PackageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(detailPackage: DetailPackage)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(detailPackages: List<DetailPackage>)

    @Update
    fun update(detailPackage: DetailPackage)

    @Delete
    fun remove(detailPackage: DetailPackage)

    @Query("DELETE FROM packages")
    fun removeAll()

    @Query("SELECT * FROM packages WHERE idGroup = :id")
    fun getPackagesByGroupId(id: Long): LiveData<List<DetailPackage>>

    @Query("SELECT * FROM packages WHERE isPending")
    fun getPendingPackages(): LiveData<List<DetailPackage>>

    @Query("SELECT * FROM packages WHERE isPending")
    fun getPendingPackagesWithoutLiveData(): List<DetailPackage>

    @Query("SELECT * FROM packages WHERE id = :id LIMIT 1")
    fun getPendingPackageById(id: Long): DetailPackage?

    @Query("UPDATE packages SET isPending = :status WHERE id = :id")
    fun updatePendingStatusById(id: Long, status: Boolean)
}