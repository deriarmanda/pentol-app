package com.karyastudio.deriarmanda.pentolapp.util

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

// main.MainActivity
const val FRAGMENT_TAG_HOME = "tag_home"
const val FRAGMENT_TAG_PACKAGE = "tag_soto_package"
const val FRAGMENT_TAG_SISIPAN = "tag_soto_sisipan"
const val FRAGMENT_TAG_PENDING = "tag_pending"

// soto.detail.DetailActivity
const val FRAGMENT_TAG_LIST = "tag_list_detail_package"
const val FRAGMENT_TAG_FORM = "tag_form_detail_package"

fun AppCompatActivity.replaceFragmentByTag(@IdRes containerId: Int, fragment: Fragment, tag: String) {
    supportFragmentManager.beginTransaction()
        .replace(containerId, fragment, tag)
        .commitAllowingStateLoss()
}