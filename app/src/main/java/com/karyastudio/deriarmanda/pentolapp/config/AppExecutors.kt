package com.karyastudio.deriarmanda.pentolapp.config

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class AppExecutors(
    val diskIO: Executor,
    val networkIO: Executor,
    val mainThread: Executor
) {

    companion object {

        private const val NETWORK_THREAD_COUNT = 3
        private var instance: AppExecutors? = null

        fun getInstance(): AppExecutors {
            return instance ?: synchronized(this) {
                instance ?: buildExecutors().also { instance = it }
            }
        }

        private fun buildExecutors(): AppExecutors {
            return AppExecutors(
                Executors.newSingleThreadExecutor(),
                Executors.newFixedThreadPool(NETWORK_THREAD_COUNT),
                MainThreadExecutor()
            )
        }
    }

    private class MainThreadExecutor : Executor {

        private val mainThreadHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable?) {
            mainThreadHandler.post(command)
        }
    }
}