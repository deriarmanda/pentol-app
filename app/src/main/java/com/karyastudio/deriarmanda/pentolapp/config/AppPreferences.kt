package com.karyastudio.deriarmanda.pentolapp.config

import android.content.Context
import android.content.SharedPreferences

class AppPreferences(val sharedPref: SharedPreferences) {

    companion object {

        @Volatile
        private var instance: AppPreferences? = null

        fun getInstance(context: Context): AppPreferences {
            return instance ?: synchronized(this) {
                instance ?: AppPreferences(buildPreferences(context)).also { instance = it }
            }
        }

        private fun buildPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences(
                PREFERENCES_NAME,
                Context.MODE_PRIVATE
            )
        }
    }

    fun isUserAlreadyLogin() = sharedPref.getBoolean(PREF_KEY_USER_STATUS, false)

    fun changeUserStatus(isLogin: Boolean) {
        val editor = sharedPref.edit()
        editor.putBoolean(PREF_KEY_USER_STATUS, isLogin)
        editor.apply()
    }
}