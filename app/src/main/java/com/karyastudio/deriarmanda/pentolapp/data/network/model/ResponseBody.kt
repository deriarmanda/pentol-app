package com.karyastudio.deriarmanda.pentolapp.data.network.model

import com.google.gson.annotations.SerializedName
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup

data class Common<T>(
    val status: String,
    val message: String,
    val data: T? = null
)

data class UserInfo(
    @SerializedName("u_id") val id: Int,
    @SerializedName("u_username") val username: String,
    @SerializedName("u_name") val name: String,
    @SerializedName("u_level") val level: String,
    @SerializedName("u_group") val group: String,
    @SerializedName("r_code") val rayonCode: String,
    @SerializedName("r_name") val rayonName: String,
    @SerializedName("u_status") val status: String
)

data class PackageGroupsResponse(
    @SerializedName("st_id") val id: Long,
    @SerializedName("rayon") val rayonName: String,
    @SerializedName("kodeRayon") val rayonCode: String,
    @SerializedName("sti_group") val group: String,
    @SerializedName("st_day") val dayWhen: String,
    @SerializedName("st_date") val dateWhen: String,
    @SerializedName("countSoTo") val countSoTo: Int,
    @SerializedName("countSudahDiperiksa") val countCheckedSoTo: Int,
    @SerializedName("countBelumDiperiksa") val countUncheckedSoTo: Int,
    @SerializedName("countSudahDisetujui") val countApprovedSoTo: Int,
    @SerializedName("countBelumDisetujui") val countUnapprovedSoTo: Int,
    @SerializedName("countUlangi") val countRetryingSoTo: Int
) {
    fun convertToLocalPackageGroupModel(): PackageGroup {
        return PackageGroup(
            id, rayonName, rayonCode, group,
            "$dayWhen, $dateWhen",
            countSoTo, countCheckedSoTo,
            countUncheckedSoTo, countApprovedSoTo,
            countUnapprovedSoTo, countRetryingSoTo
        )
    }
}

data class DetailPackageResponse(
    @SerializedName("sti_id") val id: Long,
    @SerializedName("sti_st_id") val idGroup: Long,
    @SerializedName("sti_group") val namaGroup: String,
    @SerializedName("sti_id_pemeriksa") val idPemeriksa: Long,
    @SerializedName("sti_petugas") val namaPemerikaa: String,
    @SerializedName("kodeRayon") val kodeRayon: String,
    @SerializedName("rayon") val namaRayon: String,
    @SerializedName("st_day") val hari: String,
    @SerializedName("st_date") val tanggal: String,
    @SerializedName("p_code") val pelangganKode: String,
    @SerializedName("p_nama") val pelangganNama: String,
    @SerializedName("p_alamat") val pelangganAlamat: String,
    @SerializedName("p_jenis_layanan") val pelangganJenisLayanan: String,
    @SerializedName("p_tarif") val pelangganTarif: String,
    @SerializedName("p_daya") val pelangganDaya: String,
    @SerializedName("p_nomor_meter_kwh") val pelangganNomorKwh: String,
    @SerializedName("sti_catatan") val pelangganDlpd: String,
    @SerializedName("p_nomor_gardu") val pelangganNomorGardu: String,
    @SerializedName("p_nomor_jurusan_tiang") val pelangganNomorTiang: String,
    @SerializedName("p_unitupi") val pelangganUnitUid: String,
    @SerializedName("p_unitap") val pelangganUnitUp3: String,
    @SerializedName("p_unitup") val pelangganUnitUlp: String,
    @SerializedName("p_latitude") val pelangganLatitude: String,
    @SerializedName("p_longitude") val pelangganLongitude: String,
    @SerializedName("st_day_diperiksa") val hariPeriksa: String,
    @SerializedName("sti_date_diperiksa") val tanggalPeriksa: String,
    @SerializedName("sti_hasil") val hasil: String,
    @SerializedName("sti_no_ba") val nomorBA: String,
    @SerializedName("sti_status_admin") val statusAdmin: String,
    @SerializedName("sti_kwh_meter_sebelum") val aSebelum: String,
    @SerializedName("sti_kwh_meter_sesudah") val aSesudah: String,
    @SerializedName("sti_kwh_merk_sesudah") val aMerk: String,
    @SerializedName("sti_kwh_tahun_sesudah") val aTahun: String,
    @SerializedName("sti_kwh_putaran_sesudah") val aPutaran: String,
    @SerializedName("sti_kwh_kondisi_visual_sebelum") val aKondisiSebelum: String,
    @SerializedName("sti_kwh_kondisi_visual_sesudah") val aKondisiSesudah: String,
    @SerializedName("sti_segel_terpasang_sebelum") val bSebelum: String,
    @SerializedName("sti_segel_terpasang_sesudah") val bSesudah: String,
    @SerializedName("sti_segel_jenis_sebelum") val bJenisSebelum: String,
    @SerializedName("sti_segel_jenis_sesudah") val bJenisSesudah: String,
    @SerializedName("sti_segel_kondisi_visual_sebelum") val bKondisiSebelum: String,
    @SerializedName("sti_segel_kondisi_visual_sesudah") val bKondisiSesudah: String,
    @SerializedName("sti_pembatas_sebelum") val cSebelum: String,
    @SerializedName("sti_pembatas_sesudah") val cSesudah: String,
    @SerializedName("sti_pembatas_kapasistas_sebelum") val cKapasitasSebelum: String,
    @SerializedName("sti_pembatas_kapasistas_sesudah") val cKapasitasSesudah: String,
    @SerializedName("sti_pembatas_merk_sebelum") val cMerkSebelum: String,
    @SerializedName("sti_pembatas_merk_sesudah") val cMerkSesudah: String,
    @SerializedName("sti_segel2_sebelum") val dSebelum: String,
    @SerializedName("sti_segel2_sesudah") val dSesudah: String,
    @SerializedName("sti_segel2_jenis_sebelum") val dJenisSebelum: String,
    @SerializedName("sti_segel2_jenis_sesudah") val dJenisSesudah: String,
    @SerializedName("sti_papan_meter_sebelum") val eSebelum: String,
    @SerializedName("sti_papan_meter_sesudah") val eSesudah: String,
    @SerializedName("sti_papan_jenis_sebelum") val eJenis1Sebelum: String,
    @SerializedName("sti_papan_jenis_sesudah") val eJenis1Sesudah: String,
    @SerializedName("sti_papan_kondisi_visual_sebelum") val eKondisiSebelum: String,
    @SerializedName("sti_papan_kondisi_visual_sesudah") val eKondisiSesudah: String,
    @SerializedName("sti_papan_segel_terpasang_sebelum") val eSegelSebelum: String,
    @SerializedName("sti_papan_segel_terpasang_sesudah") val eSegelSesudah: String,
    @SerializedName("sti_papan2_jenis_sebelum") val eJenis2Sebelum: String,
    @SerializedName("sti_papan2_jenis_sesudah") val eJenis2Sesudah: String,
    @SerializedName("sti_pengawatan_sebelum") val fSebelum: String,
    @SerializedName("sti_pengawatan_sesudah") val fSesudah: String,
    @SerializedName("sti_kesimpulan_hasil_pemeriksaan") val pelanggaran: String,
    @SerializedName("sti_tindakan_yang_dilakukan") val tindakan: String,
    @SerializedName("sti_foto_1") val urlFoto1: String,
    @SerializedName("sti_foto_2") val urlFoto2: String,
    @SerializedName("sti_foto_3") val urlFoto3: String,
    @SerializedName("sti_saksi_nama") val saksiNama: String,
    @SerializedName("sti_saksi_alamat") val saksiAlamat: String,
    @SerializedName("sti_saksi_nomor_kartu_identitas") val saksiNomorIdentitas: String,
    @SerializedName("sti_saksi_pekerjaan") val saksiPekerjaan: String,
    @SerializedName("sti_pengukuran_tegangan") val pengukuranTegangan: String,
    @SerializedName("sti_pengukuran_arus") val pengukuranArus: String,
    @SerializedName("sti_pengukuran_kw") val pengukuranKW: String,
    @SerializedName("sti_pengukuran_konstanta") val pengukuranKonstanta: String,
    @SerializedName("sti_pengukuran_put_detik") val pengukuranPutDetik: String,
    @SerializedName("sti_pengukuran_eror_meter") val pengukuranErrorMeter: String,
    @SerializedName("sti_pengukuran_kwh_kumulatif") val pengukuranKwhKumulatif: String,
    @SerializedName("sti_pengukuran_pemakaian_kumulatif") val pengukuranPakaiKumulatif: String,
    @SerializedName("sti_pengukuran_sisa_token") val pengukuranSisaToken: String
) {
    fun convertToLocalDetailPackageModel(): DetailPackage {
        return DetailPackage(
            id, idGroup, namaGroup, idPemeriksa, namaPemerikaa, kodeRayon, namaRayon, hari, tanggal,
            pelangganKode, pelangganNama, pelangganAlamat, pelangganJenisLayanan, pelangganTarif,
            pelangganDaya, pelangganNomorKwh, pelangganDlpd, pelangganNomorGardu, pelangganNomorTiang, pelangganUnitUid,
            pelangganUnitUp3, pelangganUnitUlp, pelangganLatitude, pelangganLongitude, hariPeriksa, tanggalPeriksa,
            hasil, nomorBA, statusAdmin, false, aSebelum, aSesudah, aMerk, aTahun, aPutaran, aKondisiSebelum,
            aKondisiSesudah, bSebelum, bSesudah, bJenisSebelum, bJenisSesudah, bKondisiSebelum, bKondisiSesudah,
            cSebelum, cSesudah, cKapasitasSebelum, cKapasitasSesudah, cMerkSebelum, cMerkSesudah,
            dSebelum, dSesudah, dJenisSebelum, dJenisSesudah, eSebelum, eSesudah, eJenis1Sebelum, eJenis1Sesudah,
            eKondisiSebelum, eKondisiSesudah, eSegelSebelum, eSegelSesudah, eJenis2Sebelum, eJenis2Sesudah,
            fSebelum, fSesudah, pelanggaran, tindakan, saksiNama, saksiAlamat, saksiNomorIdentitas, saksiPekerjaan,
            pengukuranTegangan, pengukuranArus, pengukuranKW, pengukuranKonstanta, pengukuranPutDetik,
            pengukuranErrorMeter, pengukuranKwhKumulatif, pengukuranPakaiKumulatif, pengukuranSisaToken
        )
    }
}