package com.karyastudio.deriarmanda.pentolapp.data.local.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "packages")
data class DetailPackage(
    @PrimaryKey val id: Long,
    val idGroup: Long,
    val namaGroup: String,
    val idPemeriksa: Long,
    val namaPemeriksa: String,
    val kodeRayon: String,
    val namaRayon: String,
    val hari: String,
    val tanggal: String,
    val pelangganKode: String,
    val pelangganNama: String,
    val pelangganAlamat: String,
    val pelangganJenisLayanan: String,
    val pelangganTarif: String,
    val pelangganDaya: String,
    val pelangganNomorKwh: String,
    val pelangganDlpd: String,
    val pelangganNomorGardu: String,
    val pelangganNomorTiang: String,
    val pelangganUnitUid: String,
    val pelangganUnitUp3: String,
    val pelangganUnitUlp: String,
    val pelangganLatitude: String,
    val pelangganLongitude: String,
    val hariPeriksa: String,
    val tanggalPeriksa: String,
    val hasil: String,
    val nomorBA: String,
    val statusAdmin: String,
    val isPending: Boolean = false,
    val aSebelum: String = "",
    val aSesudah: String = "",
    val aMerk: String = "",
    val aTahun: String = "",
    val aPutaran: String = "",
    val aKondisiSebelum: String = "",
    val aKondisiSesudah: String = "",
    val bSebelum: String = "",
    val bSesudah: String = "",
    val bJenisSebelum: String = "",
    val bJenisSesudah: String = "",
    val bKondisiSebelum: String = "",
    val bKondisiSesudah: String = "",
    val cSebelum: String = "",
    val cSesudah: String = "",
    val cKapasitasSebelum: String = "",
    val cKapasitasSesudah: String = "",
    val cMerkSebelum: String = "",
    val cMerkSesudah: String = "",
    val dSebelum: String = "",
    val dSesudah: String = "",
    val dJenisSebelum: String = "",
    val dJenisSesudah: String = "",
    val eSebelum: String = "",
    val eSesudah: String = "",
    val eJenis1Sebelum: String = "",
    val eJenis1Sesudah: String = "",
    val eKondisiSebelum: String = "",
    val eKondisiSesudah: String = "",
    val eSegelSebelum: String = "",
    val eSegelSesudah: String = "",
    val eJenis2Sebelum: String = "",
    val eJenis2Sesudah: String = "",
    val fSebelum: String = "",
    val fSesudah: String = "",
    val pelanggaran: String = "",
    val tindakan: String = "",
    val pathFoto1: String = "",
    val pathFoto2: String = "",
    val pathFoto3: String = "",
    val saksiNama: String = "",
    val saksiAlamat: String = "",
    val saksiNomorIdentitas: String = "",
    val saksiPekerjaan: String = "",
    val pengukuranTegangan: String = "",
    val pengukuranArus: String = "",
    val pengukuranKW: String = "",
    val pengukuranKonstanta: String = "",
    val pengukuranPutDetik: String = "",
    val pengukuranErrorMeter: String = "",
    val pengukuranKwhKumulatif: String = "",
    val pengukuranPakaiKumulatif: String = "",
    val pengukuranSisaToken: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readByte() != 0.toByte(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeLong(idGroup)
        parcel.writeString(namaGroup)
        parcel.writeLong(idPemeriksa)
        parcel.writeString(namaPemeriksa)
        parcel.writeString(kodeRayon)
        parcel.writeString(namaRayon)
        parcel.writeString(hari)
        parcel.writeString(tanggal)
        parcel.writeString(pelangganKode)
        parcel.writeString(pelangganNama)
        parcel.writeString(pelangganAlamat)
        parcel.writeString(pelangganJenisLayanan)
        parcel.writeString(pelangganTarif)
        parcel.writeString(pelangganDaya)
        parcel.writeString(pelangganNomorKwh)
        parcel.writeString(pelangganDlpd)
        parcel.writeString(pelangganNomorGardu)
        parcel.writeString(pelangganNomorTiang)
        parcel.writeString(pelangganUnitUid)
        parcel.writeString(pelangganUnitUp3)
        parcel.writeString(pelangganUnitUlp)
        parcel.writeString(pelangganLatitude)
        parcel.writeString(pelangganLongitude)
        parcel.writeString(hariPeriksa)
        parcel.writeString(tanggalPeriksa)
        parcel.writeString(hasil)
        parcel.writeString(nomorBA)
        parcel.writeString(statusAdmin)
        parcel.writeByte(if (isPending) 1 else 0)
        parcel.writeString(aSebelum)
        parcel.writeString(aSesudah)
        parcel.writeString(aMerk)
        parcel.writeString(aTahun)
        parcel.writeString(aPutaran)
        parcel.writeString(aKondisiSebelum)
        parcel.writeString(aKondisiSesudah)
        parcel.writeString(bSebelum)
        parcel.writeString(bSesudah)
        parcel.writeString(bJenisSebelum)
        parcel.writeString(bJenisSesudah)
        parcel.writeString(bKondisiSebelum)
        parcel.writeString(bKondisiSesudah)
        parcel.writeString(cSebelum)
        parcel.writeString(cSesudah)
        parcel.writeString(cKapasitasSebelum)
        parcel.writeString(cKapasitasSesudah)
        parcel.writeString(cMerkSebelum)
        parcel.writeString(cMerkSesudah)
        parcel.writeString(dSebelum)
        parcel.writeString(dSesudah)
        parcel.writeString(dJenisSebelum)
        parcel.writeString(dJenisSesudah)
        parcel.writeString(eSebelum)
        parcel.writeString(eSesudah)
        parcel.writeString(eJenis1Sebelum)
        parcel.writeString(eJenis1Sesudah)
        parcel.writeString(eKondisiSebelum)
        parcel.writeString(eKondisiSesudah)
        parcel.writeString(eSegelSebelum)
        parcel.writeString(eSegelSesudah)
        parcel.writeString(eJenis2Sebelum)
        parcel.writeString(eJenis2Sesudah)
        parcel.writeString(fSebelum)
        parcel.writeString(fSesudah)
        parcel.writeString(pelanggaran)
        parcel.writeString(tindakan)
        parcel.writeString(pathFoto1)
        parcel.writeString(pathFoto2)
        parcel.writeString(pathFoto3)
        parcel.writeString(saksiNama)
        parcel.writeString(saksiAlamat)
        parcel.writeString(saksiNomorIdentitas)
        parcel.writeString(saksiPekerjaan)
        parcel.writeString(pengukuranTegangan)
        parcel.writeString(pengukuranArus)
        parcel.writeString(pengukuranKW)
        parcel.writeString(pengukuranKonstanta)
        parcel.writeString(pengukuranPutDetik)
        parcel.writeString(pengukuranErrorMeter)
        parcel.writeString(pengukuranKwhKumulatif)
        parcel.writeString(pengukuranPakaiKumulatif)
        parcel.writeString(pengukuranSisaToken)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DetailPackage> {
        override fun createFromParcel(parcel: Parcel): DetailPackage {
            return DetailPackage(parcel)
        }

        override fun newArray(size: Int): Array<DetailPackage?> {
            return arrayOfNulls(size)
        }
    }
}