package com.karyastudio.deriarmanda.pentolapp.ui.pending

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.repository.PackageRepository
import com.karyastudio.deriarmanda.pentolapp.util.default
import id.zelory.compressor.Compressor

class PendingViewModel(private val mPackageRepo: PackageRepository) : ViewModel() {

    private val viewState = MutableLiveData<ViewState>().default(ViewState())

    fun getViewState(): LiveData<ViewState> = viewState
    fun getPendingPackages() = mPackageRepo.getPendingPackagesList()

    fun reuploadSelectedPackage(context: Context, data: DetailPackage) {
        viewState.value = currentState().copy(isLoading = true)
        mPackageRepo.reuploadPendingPackage(data, Compressor(context)) { success, error ->
            viewState.value = ViewState(false, success, error)
        }
    }

    fun reuploadAllPackages(context: Context) {
        viewState.value = currentState().copy(isLoading = true)
        mPackageRepo.reuploadPendingPackage(Compressor(context)) { success, error ->
            viewState.value = ViewState(false, success, error)
        }
    }

    fun onSuccessHandled() {
        viewState.value = currentState().copy(successMessage = null)
    }

    fun onErrorHandled() {
        viewState.value = currentState().copy(errorMessage = null)
    }

    private fun currentState() = viewState.value!!

    data class ViewState(
        val isLoading: Boolean = false,
        val successMessage: String? = null,
        val errorMessage: String? = null
    )

    class Factory(
        private val packageRepo: PackageRepository
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return PendingViewModel(packageRepo) as T
        }
    }
}
