package com.karyastudio.deriarmanda.pentolapp.ui.soto

import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup

interface PackageContract {
    fun showPackageList(list: List<PackageGroup>)
    fun showEmptyListMessage()
    fun refreshList()
    fun openSubDetailPage(selectedGroup: PackageGroup)
    fun handleError(message: String)
    fun setLoadingIndicator(active: Boolean)
}