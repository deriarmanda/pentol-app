package com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.list

import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup

interface ListContract {
    fun showPackageInfo(data: PackageGroup)
    fun showSoToList(list: List<DetailPackage>)
    fun showEmptyListMessage()
    fun refreshList()
    fun openFormDetailPage(selected: DetailPackage)
    fun openMapsApp(selected: DetailPackage)
    fun handleError(message: String)
    fun setLoadingIndicator(active: Boolean)
}