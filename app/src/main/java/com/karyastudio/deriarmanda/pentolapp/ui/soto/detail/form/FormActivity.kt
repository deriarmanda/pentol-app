package com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.form

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseActivity
import com.karyastudio.deriarmanda.pentolapp.config.RC_PICK_IMAGE_1
import com.karyastudio.deriarmanda.pentolapp.config.RC_PICK_IMAGE_2
import com.karyastudio.deriarmanda.pentolapp.config.RC_PICK_IMAGE_3
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.util.DateTimeUtils
import com.karyastudio.deriarmanda.pentolapp.util.InjectorUtils
import com.karyastudio.deriarmanda.pentolapp.util.SpinnerUtils
import com.karyastudio.deriarmanda.pentolapp.util.isChecked
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import kotlinx.android.synthetic.main.form_fragment.*

class FormActivity : BaseActivity(), FormContract {

    private lateinit var viewModel: FormViewModel

    companion object {
        private const val EXTRA_PACKAGE = "extra_detail_package"
        fun getIntent(context: Context, detailPackage: DetailPackage): Intent {
            val intent = Intent(context, FormActivity::class.java)
            intent.putExtra(EXTRA_PACKAGE, detailPackage)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.form_fragment)

        val data = intent.getParcelableExtra<DetailPackage>(EXTRA_PACKAGE)
        setupViewModel(data)
        setupObserver()

        spinner_hasil.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    4, 5 -> viewModel.setFormState(true)
                    else -> viewModel.setFormState(false)
                }
            }
        }

        title_pemeriksaan.setOnClickListener { viewModel.changeCardPemeriksaanState() }
        image_pemeriksaan_collapse.setOnClickListener { viewModel.changeCardPemeriksaanState() }

        title_saksi.setOnClickListener { viewModel.changeCardSaksiState() }
        image_saksi_collapse.setOnClickListener { viewModel.changeCardSaksiState() }

        title_pengukuran.setOnClickListener { viewModel.changeCardPengukuranState() }
        image_pengukuran_collapse.setOnClickListener { viewModel.changeCardPengukuranState() }

        button_save.setOnClickListener { processUpdate() }

        card_foto_1.setOnClickListener { pickImage(RC_PICK_IMAGE_1) }
        card_foto_2.setOnClickListener { pickImage(RC_PICK_IMAGE_2) }
        card_foto_3.setOnClickListener { pickImage(RC_PICK_IMAGE_3) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RC_PICK_IMAGE_1, RC_PICK_IMAGE_2, RC_PICK_IMAGE_3 -> {
                //handleCurrentState()
                val image = ImagePicker.getFirstImageOrNull(data)
                val target = when (requestCode) {
                    RC_PICK_IMAGE_3 -> image_foto_3
                    RC_PICK_IMAGE_2 -> image_foto_2
                    else -> image_foto_1
                }

                image?.let {
                    when (requestCode) {
                        RC_PICK_IMAGE_1 -> viewModel.setPackageFoto(0, it.path)
                        RC_PICK_IMAGE_2 -> viewModel.setPackageFoto(1, it.path)
                        RC_PICK_IMAGE_3 -> viewModel.setPackageFoto(2, it.path)
                    }

                    Glide.with(this)
                        .load(it.path)
                        .placeholder(R.drawable.ic_check_circle_gray_32dp)
                        .error(R.drawable.ic_add_a_photo_blue_32dp)
                        .into(target)
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun setFormState(isShown: Boolean) {
        layout_form_pemeriksaan.visibility = if (isShown) View.VISIBLE else View.GONE
        card_saksi.visibility = if (isShown) View.VISIBLE else View.GONE
        card_pengukuran.visibility = if (isShown) View.VISIBLE else View.GONE
    }

    override fun setCardPemeriksaanState(isExpand: Boolean) {
        form_pemeriksaan.visibility = if (isExpand) View.VISIBLE else View.GONE
        image_pemeriksaan_expand.setImageResource(
            if (isExpand) R.drawable.ic_expand_less_dark_gray_24dp
            else R.drawable.ic_expand_more_dark_gray_24dp
        )
    }

    override fun setCardSaksiState(isExpand: Boolean) {
        form_saksi.visibility = if (isExpand) View.VISIBLE else View.GONE
        image_saksi_expand.setImageResource(
            if (isExpand) R.drawable.ic_expand_less_dark_gray_24dp
            else R.drawable.ic_expand_more_dark_gray_24dp
        )
    }

    override fun setCardPengukuranState(isExpand: Boolean) {
        form_pengukuran.visibility = if (isExpand) View.VISIBLE else View.GONE
        image_pengukuran_expand.setImageResource(
            if (isExpand) R.drawable.ic_expand_less_dark_gray_24dp
            else R.drawable.ic_expand_more_dark_gray_24dp
        )
    }

    override fun showPackage(data: DetailPackage) {
        text_nama_pelanggan.text = data.pelangganNama
        text_alamat_pelanggan.text = data.pelangganAlamat
        text_unit_uid.text = data.pelangganUnitUid
        text_unit_up3.text = data.pelangganUnitUp3
        text_unit_ulp.text = data.pelangganUnitUlp
        text_tarif.text = data.pelangganTarif
        text_daya.text = data.pelangganDaya
        text_gardu.text = data.pelangganNomorGardu
        text_tiang.text = data.pelangganNomorTiang
        text_dlpd.text = data.pelangganDlpd
        text_nomor_kwh.text = data.pelangganNomorKwh
        fab_maps.setOnClickListener {
            openMapsApp(
                data.pelangganLatitude,
                data.pelangganLongitude,
                data.pelangganAlamat
            )
        }

        viewModel.setPackageFoto(0, "")
        viewModel.setPackageFoto(1, "")
        viewModel.setPackageFoto(2, "")
        if (data.isChecked()) {
            form_nomor_ba.setText(data.nomorBA)
            spinner_hasil.setSelection(SpinnerUtils.findHasilSpinnerIndexByValue(resources, data.hasil))
            // Form Pemeriksaan
            // 1.a
            spinner_kwh_sebelum.setSelection(SpinnerUtils.findAdaTidakSpinnerIndexByValue(resources, data.aSebelum))
            spinner_kwh_sesudah.setSelection(SpinnerUtils.findAdaTidakSpinnerIndexByValue(resources, data.aSesudah))
            form_merk_kwh.setText(data.aMerk)
            form_tahun_kwh.setText(data.aTahun)
            form_putaran_kwh.setText(data.aPutaran)
            spinner_kwh_kondisi_sebelum.setSelection(
                SpinnerUtils.findBaikTidakSpinnerIndexByValue(
                    resources,
                    data.aKondisiSebelum
                )
            )
            spinner_kwh_kondisi_sesudah.setSelection(
                SpinnerUtils.findBaikTidakSpinnerIndexByValue(
                    resources,
                    data.aKondisiSesudah
                )
            )
            // 1.b
            form_1b_sebelum.setText(data.bSebelum)
            form_1b_sesudah.setText(data.bSesudah)
            spinner_1b_jenis_sebelum.setSelection(
                SpinnerUtils.findPlastikMetalSpinnerIndexByValue(
                    resources,
                    data.bJenisSebelum
                )
            )
            spinner_1b_jenis_sesudah.setSelection(
                SpinnerUtils.findPlastikMetalSpinnerIndexByValue(
                    resources,
                    data.bJenisSesudah
                )
            )
            spinner_1b_kondisi_sebelum.setSelection(
                SpinnerUtils.findBaikTidakSpinnerIndexByValue(
                    resources,
                    data.bKondisiSebelum
                )
            )
            spinner_1b_kondisi_sesudah.setSelection(
                SpinnerUtils.findBaikTidakSpinnerIndexByValue(
                    resources,
                    data.bKondisiSesudah
                )
            )
            // 2.a
            spinner_2a_sebelum.setSelection(SpinnerUtils.findBaikTidakSpinnerIndexByValue(resources, data.cSebelum))
            spinner_2a_sesudah.setSelection(SpinnerUtils.findBaikTidakSpinnerIndexByValue(resources, data.cSesudah))
            form_2a_kapasitas_sebelum.setText(data.cKapasitasSebelum)
            form_2a_kapasitas_sesudah.setText(data.cKapasitasSesudah)
            form_2a_merk_sebelum.setText(data.cMerkSebelum)
            form_2a_merk_sesudah.setText(data.cMerkSesudah)
            // 2.b
            form_2b_sebelum.setText(data.dSebelum)
            form_2b_sesudah.setText(data.dSesudah)
            spinner_2b_jenis_sebelum.setSelection(
                SpinnerUtils.findPlastikMetalSpinnerIndexByValue(
                    resources,
                    data.dJenisSebelum
                )
            )
            spinner_2b_jenis_sesudah.setSelection(
                SpinnerUtils.findPlastikMetalSpinnerIndexByValue(
                    resources,
                    data.dJenisSesudah
                )
            )
            // 3.a
            spinner_3a_sebelum.setSelection(SpinnerUtils.findPakaiTidakSpinnerIndexByValue(resources, data.eSebelum))
            spinner_3a_sesudah.setSelection(SpinnerUtils.findPakaiTidakSpinnerIndexByValue(resources, data.eSesudah))
            spinner_3a_jenis_sebelum.setSelection(
                SpinnerUtils.findPlastikMetalSpinnerIndexByValue(
                    resources,
                    data.eJenis1Sebelum
                )
            )
            spinner_3a_jenis_sesudah.setSelection(
                SpinnerUtils.findPlastikMetalSpinnerIndexByValue(
                    resources,
                    data.eJenis1Sesudah
                )
            )
            spinner_3a_kondisi_sebelum.setSelection(
                SpinnerUtils.findBaikTidakSpinnerIndexByValue(
                    resources,
                    data.eKondisiSebelum
                )
            )
            spinner_3a_kondisi_sesudah.setSelection(
                SpinnerUtils.findBaikTidakSpinnerIndexByValue(
                    resources,
                    data.eKondisiSesudah
                )
            )
            form_3a_segel_sebelum.setText(data.eSegelSebelum)
            form_3a_segel_sesudah.setText(data.eSegelSesudah)
            spinner_3a_jenis_sebelum_1.setSelection(
                SpinnerUtils.findPlastikMetalSpinnerIndexByValue(
                    resources,
                    data.eJenis2Sebelum
                )
            )
            spinner_3a_jenis_sesudah_1.setSelection(
                SpinnerUtils.findPlastikMetalSpinnerIndexByValue(
                    resources,
                    data.eJenis2Sesudah
                )
            )
            // 4.a
            spinner_4a_sebelum.setSelection(SpinnerUtils.findSesuaiTidakSpinnerIndexByValue(resources, data.fSebelum))
            spinner_4a_sesudah.setSelection(SpinnerUtils.findSesuaiTidakSpinnerIndexByValue(resources, data.fSesudah))
            // Kesimpulan
            form_pelanggaran.setText(data.pelanggaran)
            form_tindakan.setText(data.tindakan)
            image_foto_1.setImageResource(R.drawable.ic_add_a_photo_blue_32dp)
            image_foto_2.setImageResource(R.drawable.ic_add_a_photo_blue_32dp)
            image_foto_3.setImageResource(R.drawable.ic_add_a_photo_blue_32dp)
            /*Glide.with(this)
                .load(data.pathFoto1)
                .error(R.drawable.ic_add_a_photo_blue_32dp)
                .into(image_foto_1)
            Glide.with(this)
                .load(data.pathFoto2)
                .error(R.drawable.ic_add_a_photo_blue_32dp)
                .into(image_foto_2)
            Glide.with(this)
                .load(data.pathFoto3)
                .error(R.drawable.ic_add_a_photo_blue_32dp)
                .into(image_foto_3)*/

            // Form Saksi
            form_saksi_nama.setText(data.saksiNama)
            form_saksi_alamat.setText(data.saksiAlamat)
            form_saksi_nomor_kartu.setText(data.saksiNomorIdentitas)
            form_saksi_pekerjaan.setText(data.saksiPekerjaan)

            // Form Pengukuran
            form_pengukuran_tegangan.setText(data.pengukuranTegangan)
            form_pengukuran_arus.setText(data.pengukuranArus)
            form_pengukuran_kw.setText(data.pengukuranKW)
            form_pengukuran_konstanta.setText(data.pengukuranKonstanta)
            form_pengukuran_put_detik.setText(data.pengukuranPutDetik)
            form_pengukuran_error_meter.setText(data.pengukuranErrorMeter)
            form_pengukuran_kwh_kumulatif.setText(data.pengukuranKwhKumulatif)
            form_pengukuran_pemakaian_kumulatif.setText(data.pengukuranPakaiKumulatif)
            form_pengukuran_sisa_token.setText(data.pengukuranSisaToken)
        } else {
            form_nomor_ba.setText("")
            spinner_hasil.setSelection(0)
            // Form Pemeriksaan
            // 1.a
            spinner_kwh_sebelum.setSelection(0)
            spinner_kwh_sesudah.setSelection(0)
            form_merk_kwh.setText("")
            form_tahun_kwh.setText("")
            form_putaran_kwh.setText("")
            spinner_kwh_kondisi_sebelum.setSelection(0)
            spinner_kwh_kondisi_sesudah.setSelection(0)
            // 1.b
            form_1b_sebelum.setText("")
            form_1b_sesudah.setText("")
            spinner_1b_jenis_sebelum.setSelection(0)
            spinner_1b_jenis_sesudah.setSelection(0)
            spinner_1b_kondisi_sebelum.setSelection(0)
            spinner_1b_kondisi_sesudah.setSelection(0)
            // 2.a
            spinner_2a_sebelum.setSelection(0)
            spinner_2a_sesudah.setSelection(0)
            form_2a_kapasitas_sebelum.setText("")
            form_2a_kapasitas_sesudah.setText("")
            form_2a_merk_sebelum.setText("")
            form_2a_merk_sesudah.setText("")
            // 2.b
            form_2b_sebelum.setText("")
            form_2b_sesudah.setText("")
            spinner_2b_jenis_sebelum.setSelection(0)
            spinner_2b_jenis_sesudah.setSelection(0)
            // 3.a
            spinner_3a_sebelum.setSelection(0)
            spinner_3a_sesudah.setSelection(0)
            spinner_3a_jenis_sebelum.setSelection(0)
            spinner_3a_jenis_sesudah.setSelection(0)
            spinner_3a_kondisi_sebelum.setSelection(0)
            spinner_3a_kondisi_sesudah.setSelection(0)
            form_3a_segel_sebelum.setText("")
            form_3a_segel_sesudah.setText("")
            spinner_3a_jenis_sebelum_1.setSelection(0)
            spinner_3a_jenis_sesudah_1.setSelection(0)
            // 4.a
            spinner_4a_sebelum.setSelection(0)
            spinner_4a_sesudah.setSelection(0)
            // Kesimpulan
            form_pelanggaran.setText("")
            form_tindakan.setText("")
            image_foto_1.setImageResource(R.drawable.ic_add_a_photo_blue_32dp)
            image_foto_2.setImageResource(R.drawable.ic_add_a_photo_blue_32dp)
            image_foto_3.setImageResource(R.drawable.ic_add_a_photo_blue_32dp)

            // Form Saksi
            form_saksi_nama.setText("")
            form_saksi_alamat.setText("")
            form_saksi_nomor_kartu.setText("")
            form_saksi_pekerjaan.setText("")

            // Form Pengukuran
            form_pengukuran_tegangan.setText("")
            form_pengukuran_arus.setText("")
            form_pengukuran_kw.setText("")
            form_pengukuran_konstanta.setText("")
            form_pengukuran_put_detik.setText("")
            form_pengukuran_error_meter.setText("")
            form_pengukuran_kwh_kumulatif.setText("")
            form_pengukuran_pemakaian_kumulatif.setText("")
            form_pengukuran_sisa_token.setText("")
        }
    }

    override fun processUpdate() {
        if (viewModel.validateBeforeUpdate()) {
            viewModel.fillPackageWithForm(collectData())
            viewModel.updatePackage(applicationContext)
        } else Toast.makeText(this, R.string.update_validate_error, Toast.LENGTH_LONG).show()
    }

    override fun openMapsApp(lat: String, long: String, label: String) {
        val uri = Uri.parse("geo:$lat,$long?q=$lat,$long($label)")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.setPackage("com.google.android.apps.maps")

        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        } else {
            showMapsNotFoundErrorDialog()
        }
    }

    override fun pickImage(requestCode: Int) {
        ImagePicker.create(this)
            .returnMode(ReturnMode.CAMERA_ONLY)
            .single()
            .toolbarImageTitle("Pilih Foto")
            .theme(R.style.AppTheme_CustomImagePicker)
            .start(requestCode)
    }

    override fun onProcessUpdateError(message: String) {
        showUpdateErrorDialog(message)
        viewModel.onUpdateErrorHandled()
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
        button_save.isEnabled = !active
        /*if (active) {
            indicatorLoading = LovelyProgressDialog(requireContext())
                .setIcon(R.drawable.ic_cloud_download_white_36dp)
                .setTitle(R.string.dialog_title_loading)
                .setTopColorRes(R.color.accent)
                .show()
        }
        else indicatorLoading.dismiss()*/
    }

    private fun setupViewModel(detailPackage: DetailPackage) {
        val factory = InjectorUtils.provideDetailPackageViewModelFactory(this, detailPackage)
        viewModel = ViewModelProviders.of(this, factory).get(FormViewModel::class.java)
    }

    private fun setupObserver() {
        viewModel.getViewState().observe(this, Observer { state ->
            setFormState(state.isFormShown)
            setCardPemeriksaanState(state.isCardPemeriksaanExpanded)
            setCardSaksiState(state.isCardSaksiExpanded)
            setCardPengukuranState(state.isCardPengukuranExpanded)

            setLoadingIndicator(state.isLoading)
            state.updateErrorMessage?.let { onProcessUpdateError(it) }
            if (state.isUpdateSuccess) {
                Toast.makeText(
                    this,
                    R.string.update_message_success,
                    Toast.LENGTH_LONG
                ).show()
                viewModel.onUpdateSuccessHandled()
                finish()
            }
        })
        viewModel.getUpdatedPackage().observe(this, Observer { data ->
            data?.let { showPackage(it) }
        })
    }

    private fun collectData(): DetailPackage {
        val current = viewModel.getUpdatedPackage().value!!
        return when (spinner_hasil.selectedItemPosition) {
            4, 5 -> {
                current.copy(
                    hariPeriksa = DateTimeUtils.getTodayString(),
                    tanggalPeriksa = DateTimeUtils.getTodayDateString(),
                    hasil = spinner_hasil.selectedItem.toString(),
                    nomorBA = form_nomor_ba.text?.toString() ?: "",
                    aSebelum = spinner_kwh_sebelum.selectedItem.toString(),
                    aSesudah = spinner_kwh_sesudah.selectedItem.toString(),
                    aMerk = form_merk_kwh.text?.toString() ?: "",
                    aTahun = form_tahun_kwh.text?.toString() ?: "",
                    aPutaran = form_putaran_kwh.text?.toString() ?: "",
                    aKondisiSebelum = spinner_kwh_kondisi_sebelum.selectedItem.toString(),
                    aKondisiSesudah = spinner_kwh_kondisi_sesudah.selectedItem.toString(),
                    bSebelum = form_1b_sebelum.text?.toString() ?: "",
                    bSesudah = form_1b_sesudah.text?.toString() ?: "",
                    bJenisSebelum = spinner_1b_jenis_sebelum.selectedItem.toString(),
                    bJenisSesudah = spinner_1b_jenis_sesudah.selectedItem.toString(),
                    bKondisiSebelum = spinner_1b_kondisi_sebelum.selectedItem.toString(),
                    bKondisiSesudah = spinner_1b_kondisi_sesudah.selectedItem.toString(),
                    cSebelum = spinner_2a_sebelum.selectedItem.toString(),
                    cSesudah = spinner_2a_sesudah.selectedItem.toString(),
                    cKapasitasSebelum = form_2a_kapasitas_sebelum.text?.toString() ?: "",
                    cKapasitasSesudah = form_2a_kapasitas_sesudah.text?.toString() ?: "",
                    cMerkSebelum = form_2a_merk_sebelum.text?.toString() ?: "",
                    cMerkSesudah = form_2a_merk_sesudah.text?.toString() ?: "",
                    dSebelum = form_2b_sebelum.text?.toString() ?: "",
                    dSesudah = form_2b_sesudah.text?.toString() ?: "",
                    dJenisSebelum = spinner_2b_jenis_sebelum.selectedItem.toString(),
                    dJenisSesudah = spinner_2b_jenis_sesudah.selectedItem.toString(),
                    eSebelum = spinner_3a_sebelum.selectedItem.toString(),
                    eSesudah = spinner_3a_sesudah.selectedItem.toString(),
                    eJenis1Sebelum = spinner_3a_jenis_sebelum.selectedItem.toString(),
                    eJenis1Sesudah = spinner_3a_jenis_sesudah.selectedItem.toString(),
                    eKondisiSebelum = spinner_3a_kondisi_sebelum.selectedItem.toString(),
                    eKondisiSesudah = spinner_3a_kondisi_sesudah.selectedItem.toString(),
                    eSegelSebelum = form_3a_segel_sebelum.text?.toString() ?: "",
                    eSegelSesudah = form_3a_segel_sesudah.text?.toString() ?: "",
                    eJenis2Sebelum = spinner_3a_jenis_sebelum_1.selectedItem.toString(),
                    eJenis2Sesudah = spinner_3a_jenis_sesudah_1.selectedItem.toString(),
                    fSebelum = spinner_4a_sebelum.selectedItem.toString(),
                    fSesudah = spinner_4a_sesudah.selectedItem.toString(),
                    pelanggaran = form_pelanggaran.text?.toString() ?: "",
                    tindakan = form_tindakan.text?.toString() ?: "",
                    saksiNama = form_saksi_nama.text?.toString() ?: "",
                    saksiAlamat = form_saksi_alamat.text?.toString() ?: "",
                    saksiNomorIdentitas = form_saksi_nomor_kartu.text?.toString() ?: "",
                    saksiPekerjaan = form_saksi_pekerjaan.text?.toString() ?: "",
                    pengukuranTegangan = form_pengukuran_tegangan.text?.toString() ?: "",
                    pengukuranArus = form_pengukuran_arus.text?.toString() ?: "",
                    pengukuranKW = form_pengukuran_kw.text?.toString() ?: "",
                    pengukuranKonstanta = form_pengukuran_konstanta.text?.toString() ?: "",
                    pengukuranPutDetik = form_pengukuran_put_detik.text?.toString() ?: "",
                    pengukuranErrorMeter = form_pengukuran_error_meter.text?.toString() ?: "",
                    pengukuranKwhKumulatif = form_pengukuran_kwh_kumulatif.text?.toString() ?: "",
                    pengukuranPakaiKumulatif = form_pengukuran_pemakaian_kumulatif.text?.toString() ?: "",
                    pengukuranSisaToken = form_pengukuran_sisa_token.text?.toString() ?: ""
                )
            }
            else -> {
                current.copy(
                    hariPeriksa = DateTimeUtils.getTodayString(),
                    tanggalPeriksa = DateTimeUtils.getTodayDateString(),
                    hasil = spinner_hasil.selectedItem.toString(),
                    nomorBA = form_nomor_ba.text?.toString() ?: ""
                )
            }
        }
    }

    private fun showMapsNotFoundErrorDialog() {
        val dialog = LovelyStandardDialog(this)
            .setTopColorRes(R.color.material_red)
            .setButtonsColorRes(R.color.accent)
            .setIcon(R.drawable.ic_error_outline_white_36dp)
            .setTitle(R.string.soto_error_maps_title)
            .setMessage(R.string.soto_error_maps_message)
            .setCancelable(true)

        dialog.setPositiveButton(R.string.action_ok) {
            dialog.dismiss()
        }.show()
    }

    private fun showUpdateErrorDialog(message: String) {
        val dialog = LovelyStandardDialog(this)
            .setTopColorRes(R.color.material_red)
            .setButtonsColorRes(R.color.accent)
            .setIcon(R.drawable.ic_error_outline_white_36dp)
            .setTitle(R.string.dialog_title_error)
            .setMessage(message)
            .setCancelable(true)

        dialog.setPositiveButton(R.string.action_coba_lagi) {
            viewModel.updatePackage(applicationContext)
        }
        dialog.setNeutralButton(R.string.action_send_later) {
            Toast.makeText(this, R.string.update_message_saved_locally, Toast.LENGTH_SHORT).show()
            finish()
        }.show()
    }
}
