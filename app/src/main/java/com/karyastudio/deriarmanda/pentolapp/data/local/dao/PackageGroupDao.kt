package com.karyastudio.deriarmanda.pentolapp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup

@Dao
interface PackageGroupDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun savePackageGroup(packageGroup: PackageGroup)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(packageGroup: List<PackageGroup>)

    @Delete
    fun removePackageGroup(packageGroup: PackageGroup)

    @Query("DELETE FROM package_groups")
    fun removeAll()

    @Query("SELECT * FROM package_groups")
    fun getPackageGroups(): LiveData<List<PackageGroup>>

}