package com.karyastudio.deriarmanda.pentolapp.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.karyastudio.deriarmanda.pentolapp.config.AppExecutors
import com.karyastudio.deriarmanda.pentolapp.config.AppPreferences
import com.karyastudio.deriarmanda.pentolapp.data.local.dao.UserDao
import com.karyastudio.deriarmanda.pentolapp.data.local.model.User
import com.karyastudio.deriarmanda.pentolapp.data.network.ClientBuilder
import com.karyastudio.deriarmanda.pentolapp.data.network.model.AuthUser
import com.karyastudio.deriarmanda.pentolapp.data.network.model.Common
import com.karyastudio.deriarmanda.pentolapp.data.network.model.UserInfo
import com.karyastudio.deriarmanda.pentolapp.data.network.service.UserService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class UserRepository private constructor(
    private val userDao: UserDao,
    private val packageRepo: PackageRepository,
    private val preferences: AppPreferences,
    private val executors: AppExecutors
) {

    private val userService = ClientBuilder.build(UserService::class.java)
    private val userProfileLiveData = userDao.getUser()
    private var userProfile: User? = null
    val userStatus: MutableLiveData<Boolean> = MutableLiveData()

    init {
        userStatus.value = preferences.isUserAlreadyLogin()
        userProfileLiveData.observeForever {
            it?.let { userProfile = it }
        }
    }

    companion object {

        private const val TAG = "UserRepository"
        @Volatile
        private var instance: UserRepository? = null

        fun getInstance(
            userDao: UserDao,
            packageRepo: PackageRepository,
            preferences: AppPreferences,
            executors: AppExecutors
        ): UserRepository {
            return instance ?: synchronized(this) {
                instance ?: UserRepository(
                    userDao, packageRepo, preferences, executors
                ).also { instance = it }
            }
        }
    }

    fun isUserAlreadyLogin(): Boolean = userStatus.value!!

    fun getObservableUserProfile() = userProfileLiveData

    fun getUserProfile() = userProfile

    fun login(username: String, password: String, onFinished: (name: String?, error: String?) -> Unit) {
        val user = AuthUser(username, password)
        userService.authenticate(user).enqueue(
            object : Callback<Common<UserInfo>> {
                override fun onFailure(call: Call<Common<UserInfo>>, t: Throwable) {
                    Log.d(TAG, "onFailure -> $t")
                    if (t is IOException) onFinished(null, t.localizedMessage)
                    else onFinished(null, "Terjadi kesalahan, silahkan coba lagi.")
                }

                override fun onResponse(call: Call<Common<UserInfo>>, response: Response<Common<UserInfo>>) {
                    if (response.isSuccessful) {
                        val data = response.body()?.data
                        if (data != null) {
                            saveUserInfo(
                                User(
                                    data.id, data.username, data.name,
                                    data.level, data.group,
                                    "${data.rayonName} (${data.rayonCode})", data.status
                                )
                            )
                            preferences.changeUserStatus(true)
                            userStatus.value = true
                            onFinished(data.name, null)
                        } else onFinished(null, "Terjadi kesalahan, silahkan coba lagi.")
                    }
                }
            }
        )
    }

    fun logout() {
        executors.diskIO.execute {
            userDao.removeUser()
            packageRepo.clearAllLocalData()
            preferences.changeUserStatus(false)
            userStatus.postValue(false)
        }
    }

    private fun saveUserInfo(user: User) {
        executors.diskIO.execute {
            userDao.removeUser()
            userDao.saveUser(user)
        }
    }
}