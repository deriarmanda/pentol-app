package com.karyastudio.deriarmanda.pentolapp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.karyastudio.deriarmanda.pentolapp.data.local.model.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveUser(user: User)

    @Query("DELETE FROM users")
    fun removeUser()

    @Query("SELECT * FROM users LIMIT 1")
    fun getUser(): LiveData<User>

    @Query("SELECT `group` FROM users LIMIT 1")
    fun getUserGroup(): String
}