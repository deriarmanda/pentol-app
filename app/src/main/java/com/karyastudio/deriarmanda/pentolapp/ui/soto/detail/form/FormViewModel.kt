package com.karyastudio.deriarmanda.pentolapp.ui.soto.detail.form

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.repository.PackageRepository
import com.karyastudio.deriarmanda.pentolapp.data.repository.UserRepository
import com.karyastudio.deriarmanda.pentolapp.util.default
import id.zelory.compressor.Compressor

class FormViewModel(
    private val mPackageRepo: PackageRepository,
    private val mUserRepo: UserRepository,
    selectedPackage: DetailPackage
) : ViewModel() {

    private val viewState = MutableLiveData<ViewState>().default(ViewState())
    private val detailPackage = MutableLiveData<DetailPackage>().default(selectedPackage)
    private val listFoto = arrayOf(
        selectedPackage.pathFoto1,
        selectedPackage.pathFoto2,
        selectedPackage.pathFoto3
    )

    fun getViewState(): LiveData<ViewState> = viewState
    fun getUpdatedPackage(): LiveData<DetailPackage> = detailPackage

    fun setPackageFoto(index: Int, path: String) {
        listFoto[index] = path
    }

    fun fillPackageWithForm(updated: DetailPackage) {
        val user = mUserRepo.getUserProfile()
        val userId: Long = user?.id?.toLong() ?: -1
        val userName = user?.name ?: "-"
        detailPackage.value = currentPackage().copy(
            idPemeriksa = userId,
            namaPemeriksa = userName,
            hariPeriksa = updated.hariPeriksa,
            tanggalPeriksa = updated.tanggalPeriksa,
            hasil = updated.hasil,
            nomorBA = updated.nomorBA,
            aSebelum = updated.aSebelum,
            aSesudah = updated.aSesudah,
            aMerk = updated.aMerk,
            aTahun = updated.aTahun,
            aPutaran = updated.aPutaran,
            aKondisiSebelum = updated.aKondisiSebelum,
            aKondisiSesudah = updated.aKondisiSesudah,
            bSebelum = updated.bSebelum,
            bSesudah = updated.bSesudah,
            bJenisSebelum = updated.bJenisSebelum,
            bJenisSesudah = updated.bJenisSesudah,
            bKondisiSebelum = updated.bKondisiSebelum,
            bKondisiSesudah = updated.bKondisiSesudah,
            cSebelum = updated.cSebelum,
            cSesudah = updated.cSesudah,
            cKapasitasSebelum = updated.cKapasitasSebelum,
            cKapasitasSesudah = updated.cKapasitasSesudah,
            cMerkSebelum = updated.cMerkSebelum,
            cMerkSesudah = updated.cMerkSesudah,
            dSebelum = updated.dSebelum,
            dSesudah = updated.dSesudah,
            dJenisSebelum = updated.dJenisSebelum,
            dJenisSesudah = updated.dJenisSesudah,
            eSebelum = updated.eSebelum,
            eSesudah = updated.eSesudah,
            eJenis1Sebelum = updated.eJenis1Sebelum,
            eJenis1Sesudah = updated.eJenis1Sesudah,
            eKondisiSebelum = updated.eKondisiSebelum,
            eKondisiSesudah = updated.eKondisiSesudah,
            eSegelSebelum = updated.eSegelSebelum,
            eSegelSesudah = updated.eSegelSesudah,
            eJenis2Sebelum = updated.eJenis2Sebelum,
            eJenis2Sesudah = updated.eJenis2Sesudah,
            fSebelum = updated.fSebelum,
            fSesudah = updated.fSesudah,
            pelanggaran = updated.pelanggaran,
            tindakan = updated.tindakan,
            pathFoto1 = listFoto[0],
            pathFoto2 = listFoto[1],
            pathFoto3 = listFoto[2],
            saksiNama = updated.saksiNama,
            saksiAlamat = updated.saksiAlamat,
            saksiNomorIdentitas = updated.saksiNomorIdentitas,
            saksiPekerjaan = updated.saksiPekerjaan,
            pengukuranTegangan = updated.pengukuranTegangan,
            pengukuranArus = updated.pengukuranArus,
            pengukuranKW = updated.pengukuranKW,
            pengukuranKonstanta = updated.pengukuranKonstanta,
            pengukuranPutDetik = updated.pengukuranPutDetik,
            pengukuranErrorMeter = updated.pengukuranErrorMeter,
            pengukuranKwhKumulatif = updated.pengukuranKwhKumulatif,
            pengukuranPakaiKumulatif = updated.pengukuranPakaiKumulatif,
            pengukuranSisaToken = updated.pengukuranSisaToken
        )
    }

    fun validateBeforeUpdate(): Boolean {
        return listFoto[0].isNotBlank() && listFoto[1].isNotBlank() && listFoto[2].isNotBlank()
    }

    fun updatePackage(context: Context) {
        viewState.value = currentState().copy(isLoading = true)
        mPackageRepo.updatePackage(currentPackage(), Compressor(context)) { error ->
            viewState.value = currentState().copy(
                isLoading = false,
                updateErrorMessage = error,
                isUpdateSuccess = error == null
            )
        }
    }

    fun setFormState(isShown: Boolean) {
        viewState.value = currentState().copy(
            isFormShown = isShown
        )
    }

    fun changeCardPemeriksaanState() {
        val cardState = !(currentState().isCardPemeriksaanExpanded)
        viewState.value = currentState().copy(
            isCardPemeriksaanExpanded = cardState
        )
    }

    fun changeCardSaksiState() {
        val cardState = !(currentState().isCardSaksiExpanded)
        viewState.value = currentState().copy(
            isCardSaksiExpanded = cardState
        )
    }

    fun changeCardPengukuranState() {
        val cardState = !(currentState().isCardPengukuranExpanded)
        viewState.value = currentState().copy(
            isCardPengukuranExpanded = cardState
        )
    }

    /*fun setWarningMessageOn() {
        viewState.value = currentState().copy(
            warningMessage = "Aplikasi baru direstart ulang, mohon periksa kembali data yang telah diinputkan untuk memastikan tidak ada data yang hilang."
        )
    }*/

    fun onUpdateErrorHandled() {
        viewState.value = currentState().copy(updateErrorMessage = null)
    }

    fun onUpdateSuccessHandled() {
        viewState.value = currentState().copy(isUpdateSuccess = false)
    }

    private fun currentState() = viewState.value!!
    private fun currentPackage() = detailPackage.value!!

    data class ViewState(
        val isFormShown: Boolean = false,
        val isCardPemeriksaanExpanded: Boolean = true,
        val isCardSaksiExpanded: Boolean = false,
        val isCardPengukuranExpanded: Boolean = false,
        val warningMessage: String? = null,
        val updateErrorMessage: String? = null,
        val isUpdateSuccess: Boolean = false,
        val isLoading: Boolean = false
    )

    class Factory(
        private val packageRepo: PackageRepository,
        private val userRepo: UserRepository,
        private val selectedPackage: DetailPackage
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return FormViewModel(packageRepo, userRepo, selectedPackage) as T
        }
    }
}
