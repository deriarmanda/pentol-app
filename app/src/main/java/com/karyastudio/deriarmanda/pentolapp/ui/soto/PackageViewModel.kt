package com.karyastudio.deriarmanda.pentolapp.ui.soto

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.karyastudio.deriarmanda.pentolapp.base.BaseViewState
import com.karyastudio.deriarmanda.pentolapp.data.repository.PackageRepository
import com.karyastudio.deriarmanda.pentolapp.data.repository.UserRepository
import com.karyastudio.deriarmanda.pentolapp.util.default

class PackageViewModel(
    private val mUserRepo: UserRepository,
    private val mPackageRepo: PackageRepository
) : ViewModel() {

    private val viewState = MutableLiveData<BaseViewState>().default(BaseViewState())

    fun getViewState(): LiveData<BaseViewState> = viewState

    fun getPackageGroups() = mPackageRepo.getLocalPackageGroups()

    fun refreshPackageGroups() {
        viewState.value = currentState().copy(isLoading = true)
        val group = mUserRepo.getUserProfile()?.group ?: "-"
        mPackageRepo.refreshPackageGroups(group) {
            viewState.value = currentState().copy(
                isLoading = false,
                errorMessage = it
            )
        }
    }

    fun onErrorHandled() {
        viewState.value = currentState().copy(errorMessage = null)
    }

    private fun currentState() = viewState.value!!

    class Factory(
        private val userRepo: UserRepository,
        private val packageRepo: PackageRepository
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return PackageViewModel(userRepo, packageRepo) as T
        }
    }
}
