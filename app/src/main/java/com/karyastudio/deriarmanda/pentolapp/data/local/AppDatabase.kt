package com.karyastudio.deriarmanda.pentolapp.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.karyastudio.deriarmanda.pentolapp.config.DATABASE_NAME
import com.karyastudio.deriarmanda.pentolapp.config.DATABASE_VERSION
import com.karyastudio.deriarmanda.pentolapp.data.local.dao.PackageDao
import com.karyastudio.deriarmanda.pentolapp.data.local.dao.PackageGroupDao
import com.karyastudio.deriarmanda.pentolapp.data.local.dao.UserDao
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import com.karyastudio.deriarmanda.pentolapp.data.local.model.PackageGroup
import com.karyastudio.deriarmanda.pentolapp.data.local.model.User

@Database(
    entities = [User::class, PackageGroup::class, DetailPackage::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun packageGroupDao(): PackageGroupDao
    //abstract fun detailPackageDao(): DetailPackageDao
    abstract fun packageDao(): PackageDao

    companion object {

        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                DATABASE_NAME
            ).build()
        }
    }
}