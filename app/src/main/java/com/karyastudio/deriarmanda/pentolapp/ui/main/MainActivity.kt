package com.karyastudio.deriarmanda.pentolapp.ui.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseActivity
import com.karyastudio.deriarmanda.pentolapp.config.RC_LOGIN_PAGE
import com.karyastudio.deriarmanda.pentolapp.data.repository.UserRepository
import com.karyastudio.deriarmanda.pentolapp.ui.home.HomeFragment
import com.karyastudio.deriarmanda.pentolapp.ui.login.LoginActivity
import com.karyastudio.deriarmanda.pentolapp.ui.pending.PendingFragment
import com.karyastudio.deriarmanda.pentolapp.ui.sisipan.SisipanFragment
import com.karyastudio.deriarmanda.pentolapp.ui.soto.PackageFragment
import com.karyastudio.deriarmanda.pentolapp.util.*
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val fragment: Fragment?
        when (item.itemId) {
            R.id.navigation_home -> {
                prevSelectedMenuId = R.id.navigation_home
                fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_HOME) ?: HomeFragment.newInstance()
                replaceFragmentByTag(R.id.container, fragment, FRAGMENT_TAG_HOME)
                supportActionBar?.setTitle(R.string.title_home)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_package -> {
                prevSelectedMenuId = R.id.navigation_package
                fragment =
                    supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_PACKAGE) ?: PackageFragment.newInstance()
                replaceFragmentByTag(R.id.container, fragment, FRAGMENT_TAG_PACKAGE)
                supportActionBar?.setTitle(R.string.title_package)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_sisipan -> {
                prevSelectedMenuId = R.id.navigation_sisipan
                fragment =
                    supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_SISIPAN) ?: SisipanFragment.newInstance()
                replaceFragmentByTag(R.id.container, fragment, FRAGMENT_TAG_SISIPAN)
                supportActionBar?.setTitle(R.string.title_sisipan)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_pending -> {
                prevSelectedMenuId = R.id.navigation_pending
                fragment =
                    supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_PENDING) ?: PendingFragment.newInstance()
                replaceFragmentByTag(R.id.container, fragment, FRAGMENT_TAG_PENDING)
                supportActionBar?.setTitle(R.string.title_pending)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_logout -> {
                logoutDialog.show()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    private var prevSelectedMenuId = R.id.navigation_home
    private lateinit var userRepo: UserRepository
    private lateinit var logoutDialog: LovelyStandardDialog

    companion object {
        fun getIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userRepo = InjectorUtils.getUserRepository(this)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = navigation.selectedItemId

        logoutDialog = LovelyStandardDialog(this)
            .setTopColorRes(R.color.primary)
            .setButtonsColorRes(R.color.accent)
            .setIcon(R.drawable.ic_question_white_32dp)
            .setTitle(R.string.dialog_title_confirmation)
            .setMessage(R.string.main_msg_logout_confirmation)
            .setCancelable(false)
            .setPositiveButton(R.string.action_yes) { userRepo.logout() }
            .setNegativeButton(R.string.action_no) { navigation.selectedItemId = prevSelectedMenuId }

        userRepo.userStatus.observe(this, Observer { if (!it) onLogout() })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_LOGIN_PAGE && resultCode != Activity.RESULT_OK) finish()
        else super.onActivityResult(requestCode, resultCode, data)
    }

    private fun onLogout() {
        Toast.makeText(this, R.string.main_msg_on_logut, Toast.LENGTH_LONG).show()
        startActivityForResult(LoginActivity.getIntent(this), RC_LOGIN_PAGE)
    }
}
