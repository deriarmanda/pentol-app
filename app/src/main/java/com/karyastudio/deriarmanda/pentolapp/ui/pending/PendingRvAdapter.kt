package com.karyastudio.deriarmanda.pentolapp.ui.pending

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.karyastudio.deriarmanda.pentolapp.R
import com.karyastudio.deriarmanda.pentolapp.base.BaseRvAdapter
import com.karyastudio.deriarmanda.pentolapp.base.BaseViewHolder
import com.karyastudio.deriarmanda.pentolapp.data.local.model.DetailPackage
import kotlinx.android.synthetic.main.item_pending.view.*

class PendingRvAdapter(
    private val onClickListener: ((data: DetailPackage) -> Unit)
) : BaseRvAdapter<DetailPackage>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<DetailPackage> {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_pending, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(view: View) : BaseViewHolder<DetailPackage>(view) {
        override fun fetch(data: DetailPackage) {
            val res = itemView.context.resources
            itemView.text_name.text = res.getString(
                R.string.soto_text_nama_kode_pelanggan,
                data.pelangganNama, data.pelangganKode
            )
            itemView.text_address.text = data.pelangganAlamat
            itemView.text_date_checked.text = res.getString(
                R.string.pending_msg_diperiksa_pada,
                data.tanggalPeriksa
            )

            itemView.text_date.text = res.getString(
                R.string.msg_daydate,
                data.hari, data.tanggal
            )
            itemView.text_date.visibility =
                if (isDateShouldBeDisplayed()) View.VISIBLE
                else View.GONE

            itemView.button_send.setOnClickListener { onClickListener(data) }
        }

        private fun isDateShouldBeDisplayed(): Boolean {
            return if (adapterPosition == 0) true
            else list[adapterPosition].tanggal != list[adapterPosition - 1].tanggal
        }
    }
}