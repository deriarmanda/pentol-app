package com.karyastudio.deriarmanda.pentolapp.data.local.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "package_groups")
data class PackageGroup(
    @PrimaryKey val id: Long,
    val rayon: String,
    val kodeRayon: String,
    val group: String,
    val tanggal: String,
    val jumlah: Int,
    val petugasSudah: Int,
    val petugasBelum: Int,
    val adminSudah: Int,
    val adminBelum: Int,
    val adminUlangi: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(rayon)
        parcel.writeString(kodeRayon)
        parcel.writeString(group)
        parcel.writeString(tanggal)
        parcel.writeInt(jumlah)
        parcel.writeInt(petugasSudah)
        parcel.writeInt(petugasBelum)
        parcel.writeInt(adminSudah)
        parcel.writeInt(adminBelum)
        parcel.writeInt(adminUlangi)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PackageGroup> {
        override fun createFromParcel(parcel: Parcel): PackageGroup {
            return PackageGroup(parcel)
        }

        override fun newArray(size: Int): Array<PackageGroup?> {
            return arrayOfNulls(size)
        }
    }
}