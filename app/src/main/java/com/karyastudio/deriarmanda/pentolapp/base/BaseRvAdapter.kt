package com.karyastudio.deriarmanda.pentolapp.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseRvAdapter<T>() : RecyclerView.Adapter<BaseViewHolder<T>>() {

    var list: List<T> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) = holder.fetch(list[position])

    interface OnItemClickListener<T> {
        fun onClick(data: T)
    }
}