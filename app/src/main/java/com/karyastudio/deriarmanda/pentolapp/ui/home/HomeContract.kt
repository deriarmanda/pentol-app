package com.karyastudio.deriarmanda.pentolapp.ui.home

import com.karyastudio.deriarmanda.pentolapp.data.local.model.User

interface HomeContract {
    fun showUserProfile(user: User)
    fun showChart()
}